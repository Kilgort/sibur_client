//
//  RemoteOrderCell.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/8/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.
//

import UIKit

//NOTE: it is remote receipt cell was lazy to rename it
class RemoteOrderCell: UICollectionViewCell {
    static let cellNib = UINib(nibName: "RemoteOrderCell", bundle: Bundle.main)
    static let cellId = "RemoteOrderCell"
    
    @IBOutlet weak var orderItemLabel: UILabel!
    @IBOutlet weak var orderStatusImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupStyles()
    }
    
    func setOrderStatus(_ orderStatus: OrderStatus) {
        orderStatusImage.image = getOrdersStatusImage(orderStatus)
    }
    
    func setOrderItemNumber(_ orderItemNumber: Int) {
        orderItemLabel.text = "Заказ №\(orderItemNumber)"
    }
    
    fileprivate func setupStyles() {
        //styles here
    }
}
