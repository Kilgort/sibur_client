//
//  InfoViewController.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/6/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.
//

import UIKit

//Using it for info popover on cocktails screen
class InfoViewController: UIViewController {
    fileprivate let infoLabel: UILabel = UILabel()
    
    var infoText: String? {
        didSet {
            infoLabel.text = infoText
        }
    }
    
    override func viewDidLoad() {
        self.view.backgroundColor = volumeTextColor
        setupInfoLabel()
    }
    
    fileprivate func setupInfoLabel() {
        infoLabel.frame = CGRect(x: 5, y: 5, width: preferredContentSize.width, height: preferredContentSize.width)
        infoLabel.numberOfLines = 0
        infoLabel.text = infoText
        infoLabel.textColor = UIColor.white
        infoLabel.sizeToFit()

        self.view.addSubview(infoLabel)
    }
}
