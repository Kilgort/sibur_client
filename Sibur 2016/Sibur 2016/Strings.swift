//
//  Strings.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/3/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.
//

import Foundation

let orderAddedAlertTitle = "Заказ добавлен"

func orderAddedAlertMessage(_ itemName: String) -> String {
    return "\(itemName) добавлен к текущему заказу."
}
