//
//  SetupDataService.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/22/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.
//

import Foundation
import Alamofire

private let localStorage = UserDefaults.standard
let deviceIdKey = "device_id"
let deviceNumberKey = "device_number"
let vendorIdKey = "vendor_id"
let savedIpKey = "saved_ip"

//let 

class SetupDataService {
    static func isDeviceRegistered(isRegisteredCallback: @escaping (Bool) -> ()) {
        if !isDeviceRegisteredLocally() {
            isRegisteredCallback(false)
        } else {
            
            if let deviceId = localStorage.string(forKey: deviceIdKey) {
                isDeviceIdRelevantRemotelly(deviceId) {
                    isRelevant in
                    
                    isRegisteredCallback(isRelevant)
                }
            }
                
            else {
                isRegisteredCallback(false)
            }
        }
    }
    
    static func isDeviceIdRelevantRemotelly(_ deviceId: String, isRelevantCallback: @escaping (Bool) -> ()) {
        let deviceIdRequest = createResourceUrl(SetupDataService.getServerIp(), resource: "device") + "/" + deviceId
        
        Alamofire.request(deviceIdRequest, method: .get)
            .responseJSON {
                response in
                
                if let json = response.result.value {
                    let jsonDict = json as! Dictionary<NSObject, AnyObject>
                    
                    if jsonDict.isEmpty {
                        isRelevantCallback(false)
                    }
                    else {
                        if let message = jsonDict["message" as NSObject] {
                            print(message)
                            isRelevantCallback(false)
                        }
                        else {
                            isRelevantCallback(true)
                        }
                    }
                }
        }
        
    }
    
    static func isDeviceRegisteredLocally() -> Bool {
        let deviceId = localStorage.string(forKey: deviceIdKey)
        let deviceNumber = localStorage.integer(forKey: deviceNumberKey)
        return deviceId != nil && deviceNumber != 0
    }
    
    static func saveDeviceRegistration(_ deviceInfo: DeviceDto) {
        let deviceId = deviceInfo.id
        let deviceNumber = deviceInfo.number
        let vendorId = deviceInfo.vendorId
        
        localStorage.set(deviceId, forKey: deviceIdKey)
        localStorage.set(vendorId, forKey: vendorIdKey)
        localStorage.set(deviceNumber, forKey: deviceNumberKey)
        
        localStorage.synchronize()
    }
    
    static func getDeviceRegistration() -> DeviceDto? {
        if isDeviceRegisteredLocally() {
            let deviceId = localStorage.string(forKey: deviceIdKey)
            let deviceNumber = localStorage.integer(forKey: deviceNumberKey)
            let vendorId = localStorage.string(forKey: vendorIdKey)
            
            return DeviceDto(id: deviceId!, number: deviceNumber, vendorId: vendorId!)
        }
        
        return nil
    }
    
    static func saveServerIp(_ serverIp: String) {
        localStorage.set(serverIp, forKey: savedIpKey)
        
        localStorage.synchronize()
    }
    
    static func getServerIp() -> String {
        if let serverIp = localStorage.string(forKey: savedIpKey) {
            return serverIp
        }
        
        return "127.0.0.1" //default
    }
 }

