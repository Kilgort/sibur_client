//
//  Syrup.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 12/26/15.
//  Copyright © 2015 Oleg Tyshchenko. All rights reserved.
//

import Foundation

public enum Syrup : String {
    case None = "без сиропа"
    case Caramel = "карамельный"
    case Vanil = "ванильный"
    case Chocolate = "шоколадный"
    case Walnut = "ореховый"
}
