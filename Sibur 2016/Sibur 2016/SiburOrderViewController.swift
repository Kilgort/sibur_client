//
//  SiburOrderViewController.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 12/22/15.
//  Copyright © 2015 Oleg Tyshchenko. All rights reserved.
//

import UIKit


/**
 Base ViewController for pages with ordering functionality
 Encapsulates logic for making, sending and checking orders
 */
class SiburOrderViewController: UIViewController {
    fileprivate let receiptService = ReceiptService.sharedInstance
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func addItemToReceipt(_ item: SiburItem) {
        receiptService.addItemToReceipt(item)
        showToast("Заказ добавлен", delay: 1)
    }
    
    func counterNamedFunction(_ itemName: String, itemFactoryFunction: @escaping () -> SiburItem?) -> (CountDirection, Int) -> () {
        return {
            countDirection, itemCount in
            let receiptService = ReceiptService.sharedInstance
            let idxOfItemChangingCount = receiptService.indexOfItemInReceipt(itemName)
            
            if idxOfItemChangingCount < 0 {
                receiptService.addItemToReceipt(itemFactoryFunction()!)
                showToast("Заказ добавлен", delay: 1)
            }
            else {
                switch countDirection {
                case CountDirection.decrement:
                    receiptService.decrementItemAtIndex(idxOfItemChangingCount)
                    
                    break
                case CountDirection.increment:
                    receiptService.incrementItemAtIndex(idxOfItemChangingCount)
                    showToast("Заказ добавлен", delay: 1)
                    break
                }
            }
        }
    }
}
