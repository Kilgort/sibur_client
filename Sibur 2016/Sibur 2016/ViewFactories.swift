//
//  ViewFactories.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/3/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.
//

import Foundation
import UIKit

private let itemCellBaseSize = 44

func createSelectionPopover(_ popoverView: UIButton, selectionItems: [String], onSelection: ((String) -> ())?) -> UIViewController? {
    if(selectionItems.count < 1) {
        return nil
    }
    
    let selectionPopoverTableViewController = createAndInitSelectionPopoverTableViewController(selectionItems.count)
    
    return selectionPopoverTableViewController
}


private func createAndInitSelectionPopoverTableViewController(_ itemsCount: Int) -> UITableViewController {
    let tableViewController = UITableViewController()
    tableViewController.modalPresentationStyle = UIModalPresentationStyle.popover
    tableViewController.preferredContentSize = CGSize(width: 200, height: CGFloat(itemsCount*itemCellBaseSize))
    tableViewController.tableView.isScrollEnabled = false
    tableViewController.tableView.separatorStyle = .none
    
    return tableViewController
}
