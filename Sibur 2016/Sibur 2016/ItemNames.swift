//
//  ItemNames.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 12/17/15.
//  Copyright © 2015 Oleg Tyshchenko. All rights reserved.
//

import Foundation


let TeaItemNames = ["Чай черный классический", "Чай зеленый классический", "Чай черный с бергамотом", "Чай зеленый с бергамотом"]

let CoffeeItemNames = ["Капучино", "Латтэ", "Американо", "Эспрессо"]

let ChocolateItemNames = ["Счастье", "Интересе", "Безмятежность", "Удовольствие", "Радость"]

let CocktailItemNames = ["Счастье", "Интересе", "Удовольствие", "Радость"]

let BeerItemNames = [ "Безмятежность" ]