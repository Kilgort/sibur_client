//
//  Ingridient.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 12/19/15.
//  Copyright © 2015 Oleg Tyshchenko. All rights reserved.
//

import Foundation

public enum Ingridient {
    case milk
    case sugar
}
