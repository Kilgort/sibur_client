//  ReceiptRemoteServices.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/20/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.
//

import Foundation
import Alamofire

//TODO: do I need to react to errors
class ReceiptRemoteService {
    static func getReceipts(_ onReceieve: @escaping ([RemoteReceiptDto]?) -> ()) {
        let vendorId = SetupDataService.getDeviceRegistration()!.vendorId
        let receiptGetUrl = createResourceUrl(SetupDataService.getServerIp(), resource: "receipt") + "?device_id=\(vendorId)"
        Alamofire.request(receiptGetUrl, method: .get)
            .responseJSON { response in
                
                if let json = response.result.value {
                    let jsonDict = json as! Dictionary<NSObject, AnyObject>
                    let jsonResults = jsonDict["results" as NSObject] as! NSArray
                    let receipts = jsonResults.map
                        { jsonReceipt in mapJsonToRemoteReceipt(jsonReceipt as! Dictionary<NSObject, AnyObject>) }
                    onReceieve(receipts)
                }
                else {
                    onReceieve(nil)
                }
                
        }
    }
    
    static func postReceipt(_ receipt: Receipt, onPost: @escaping (Bool) -> ()) {
        let receiptPostUrl = createResourceUrl(SetupDataService.getServerIp(), resource: "receipt")
        let vendorId = SetupDataService.getDeviceRegistration()!.vendorId
        let receiptParamaters = receiptToParameters(receipt, deviceId: vendorId)
        
        Alamofire.request(receiptPostUrl, method: .post, parameters: receiptParamaters, encoding: JSONEncoding.default)
            .responseJSON { response in
            if let json = response.result.value {
//                let jsonDict = json as! NSMutableDictionary
                
                onPost(true)
            }
            else {
                onPost(false)
            }
        }
    }
}
