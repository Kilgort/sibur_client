//
//  Order.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/8/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.
//

import Foundation

class Order {
    var orderItem: SiburItem

    //Order count is controlled by receipt, as those two items are tightly coupled
    var orderCount: Int = 1
    
    var orderName: String {
        get {
            return orderItem.name
        }
    }
    
    var orderTitle: String {
        get {
            return orderItem.fullTitle
        }
    }
    
    init(item: SiburItem) {
        self.orderItem = item
    }
}