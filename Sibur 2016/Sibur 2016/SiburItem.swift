//  SiburItem.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 12/18/15.
//  Copyright © 2015 Oleg Tyshchenko. All rights reserved.
//

import Foundation

open class SiburItem  { //NSObject, NSCoding
    fileprivate var _name : String
    
    init(name: String) {
        self._name = name
    }
    
    var name: String {
        get {
            return _name
        }
    }
    
    var fullTitle: String = ""
}
