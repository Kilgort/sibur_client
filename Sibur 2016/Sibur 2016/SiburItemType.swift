//
//  SiburItemType.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 12/17/15.
//  Copyright © 2015 Oleg Tyshchenko. All rights reserved.
//

import Foundation

public enum BeerType: Int {
    case light = 0
    case dark = 1
}

public enum SiburItemType {
    case beer(BeerType)
    case chocolate
    case coffee
    case cocktail
    case tea
}
