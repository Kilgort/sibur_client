//
//  HotBeverage.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 12/19/15.
//  Copyright © 2015 Oleg Tyshchenko. All rights reserved.
//

import Foundation

open class HotBeverage: SiburItem {
    open var withSugar : Bool
    
    init(name: String, withSugar: Bool) {
        self.withSugar = withSugar
        super.init(name: name)
    }
}
