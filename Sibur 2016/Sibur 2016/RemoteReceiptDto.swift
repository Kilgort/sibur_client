//
//  RemoteReceipt.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/20/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.
//

import Foundation

class RemoteReceiptDto {
    let id: String
    let created: Date?
    let number: Int
    let status: String
    let orders: [OrderDto]

    init(id: String, created: Date?, number: Int, status: String, orders: [OrderDto]) {
        self.id = id
        self.created = created
        self.number = number
        self.status = status
        self.orders = orders
    }
}
