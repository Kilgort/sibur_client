//
//  CounterView.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 12/24/15.
//  Copyright © 2015 Oleg Tyshchenko. All rights reserved.
//

import UIKit

/**
 
 Sibur counter view, which adds +1 if you click plus, and -1 if you click minus.
 Minus and label are hidden if count is zero
 
*/

enum CountDirection {
    case increment
    case decrement
}

@objc class CounterView: UIView {
    fileprivate let viewName = "CounterView"
    
    @objc @IBOutlet weak var plusButton: UIButton!
    @objc @IBOutlet weak var minusButton: UIButton!
    @objc @IBOutlet weak var counterLabel: UILabel!
    
    var selfView : CounterView?
    
    fileprivate var counterCountEventHandlers : [ObjectIdentifier: ((CountDirection, Int) -> ())] = Dictionary()
    
    var counterCount : Int = 0 {
        didSet {
            let countDirection: CountDirection = oldValue < counterCount ? .increment : .decrement
            raiseCounterChange(countDirection)
        }
    }
    
    var counterLabelFontSize: Int? {
        get {
            
            if let fontSize = selfView?.counterLabel.font.pointSize {
                return Int(fontSize)
            }
            return nil
            
        }
        set {
            
            if let val = newValue {
                let cLFont = counterLabel.font
                counterLabel.font = cLFont?.withSize(CGFloat(val))
            }
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeView()
    }
    
    func subcribeOnCountChanged(_ target: AnyObject, action: @escaping (CountDirection, Int) -> ()) {
        if targetNotSubscribed(target) {
            selfView!.counterCountEventHandlers[ObjectIdentifier(target)] = action
        }
    }
    
    func unsubscribeOnCountChanged(_ target: AnyObject) {
        if !targetNotSubscribed(target) {
            selfView!.counterCountEventHandlers.removeValue(forKey: ObjectIdentifier(target))
        }
    }
    
    fileprivate func targetNotSubscribed(_ target: AnyObject) -> Bool {
        let objId = ObjectIdentifier(target)
        
        return !counterCountEventHandlers.keys.contains(objId)
    }
    
    fileprivate func raiseCounterChange(_ countDirection: CountDirection) {
        for (_, actionVal) in counterCountEventHandlers {
            actionVal(countDirection, counterCount)
        }
    }
    
    fileprivate func updateUi() {
        if(counterCount > 0) {
            showLabelAndMinus()
            counterLabel.text = String(counterCount)
        }
        else {
            hideLabelAndMinus()
            counterLabel.text = String(0)
        }
    }
    
    fileprivate func showLabelAndMinus() {
        minusButton.isHidden = false
        counterLabel.isHidden = false
    }
    
    fileprivate func hideLabelAndMinus() {
        minusButton.isHidden = true
        counterLabel.isHidden = true
    }
    
    fileprivate func initializeView() {
        if(self.subviews.count == 0) {
            let counterView = Bundle.main
                .loadNibNamed(viewName, owner: self, options: nil)?[0] as! CounterView
            self.addSubview(counterView)
            selfView = counterView
            
            setupView(counterView)
        }
    }
    
    fileprivate func setupView(_ counterView : CounterView) {
        counterView.frame = self.bounds
        
        counterView.plusButton.addTarget(counterView, action: #selector(CounterView.plusUp(_:)), for: .touchUpInside)
        counterView.minusButton.addTarget(counterView, action: #selector(CounterView.minusUp(_:)), for: .touchUpInside)
        
        counterView.updateUi()
    }
    
    func plusUp(_ sender: UIButton!) {
        counterCount = counterCount + 1
        updateUi()
    }
    
    func minusUp(_ sender: UIButton!) {
        if(counterCount > 0) {
            counterCount = counterCount - 1
        }
        updateUi()
    }
}
