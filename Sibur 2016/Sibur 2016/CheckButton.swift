//
//  CheckButton.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 12/25/15.
//  Copyright © 2015 Oleg Tyshchenko. All rights reserved.
//

import UIKit

@IBDesignable
class CheckButton: NamedButton {
    @IBInspectable
    var checkedImage: UIImage?
    
    @IBInspectable
    var unCheckedImage: UIImage?
    
    var isChecked: Bool = false {
        didSet {
            if(isChecked) {
                self.setImage(checkedImage, for: UIControlState())
            }
            else {
                self.setImage(unCheckedImage, for: UIControlState())
            }
        }
    }
    
    func toggleButton() {
        isChecked = !isChecked
    }
}
