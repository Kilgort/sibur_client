//
//  CachedImages.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 12/25/15.
//  Copyright © 2015 Oleg Tyshchenko. All rights reserved.
//

import Foundation
import UIKit
import CoreGraphics

var coffeeBackgroundImage : UIImage?
var teaBackgroundImage : UIImage?
var chocolateBackgroundImage : UIImage?
var cocktailsBackgroundImage : UIImage?

func imageNonLazyLoad(_ imageNamed: String, isPng : Bool) -> UIImage? {
    if let cgDataProvider = CGDataProvider(filename: (imageNamed as NSString).utf8String!) {
        let imageRef = isPng ? CGImage(pngDataProviderSource: cgDataProvider, decode: nil, shouldInterpolate: false, intent: .defaultIntent)
            : CGImage(jpegDataProviderSource: cgDataProvider, decode: nil, shouldInterpolate: false, intent: .defaultIntent)
        let imgWidth = imageRef?.width
        let imgHeight = imageRef?.height
        
        let bufferSize = imgWidth!*imgHeight!*4
        
        let imageBuffer : UnsafeMutablePointer<CUnsignedChar> = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: bufferSize)
        let colorSpace = CGColorSpaceCreateDeviceRGB()

        let cgContext = CGContext(data: imageBuffer,
                                  width: imgWidth!,
                                  height: imgHeight!,
                                  bitsPerComponent: 8,
                                  bytesPerRow: imgWidth!*4,
                                  space: colorSpace,
                                  bitmapInfo: CGBitmapInfo.byteOrder32Little.rawValue | CGImageAlphaInfo.premultipliedFirst.rawValue)
        
        cgContext?.draw(imageRef!, in: CGRect(x: 0, y: 0, width: imgWidth!, height: imgHeight!))
        
        if let outputImage = cgContext?.makeImage() {
            imageBuffer.deallocate(capacity: bufferSize)
            return UIImage(cgImage: outputImage)
        }
    }
    
    return nil
}

func cacheBackgroundImages() {
    if let pathToCocktailsImage = Bundle.main.path(forResource: "page_drinks.png", ofType: nil) {
        cocktailsBackgroundImage = imageNonLazyLoad(pathToCocktailsImage, isPng: true)
    }
    
    
    if let pathToCoffeeImage = Bundle.main.path(forResource: "page_coffee.png", ofType: nil) {
        coffeeBackgroundImage = imageNonLazyLoad(pathToCoffeeImage, isPng: true)
    }
    
    if let pathToTeaImage = Bundle.main.path(forResource: "page_tea.png", ofType: nil) {
        teaBackgroundImage = imageNonLazyLoad(pathToTeaImage, isPng: true)
    }
    
    if let pathToChocolateImage = Bundle.main.path(forResource: "page_deserts.png", ofType: nil) {
        chocolateBackgroundImage = imageNonLazyLoad(pathToChocolateImage, isPng: true)
    }
}

func getOrdersStatusImage(_ orderStatus: OrderStatus) -> UIImage {
    let orderImageName = orderStatus == .inProcess ? "order_inprocess" : "order_ready"
    let pathToImage = Bundle.main.path(forResource: orderImageName, ofType: "png")!
    
    return UIImage(named: pathToImage)!
}

