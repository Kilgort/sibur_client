//
//  MainPageViewController.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 12/24/15.
//  Copyright © 2015 Oleg Tyshchenko. All rights reserved.
//

import UIKit

class MainPageViewController: UIPageViewController, UIPageViewControllerDataSource {
    fileprivate let ordersViewController: OrdersViewController = OrdersViewController()
    
    fileprivate let pageContentviewControllers : [UIViewController?] =
        [CoffeePageViewController(), TeaPageViewController(), ChocolatePageViewController(), FoodPageViewController(), SnacksPageViewController(), CocktailsPageViewController(), nil]
    
    fileprivate let pagedViewControllersTypes : [AnyObject.Type] = [CoffeePageViewController.self,
                                                                    TeaPageViewController.self,
                                                                    ChocolatePageViewController.self,
                                                                    FoodPageViewController.self,
                                                                    SnacksPageViewController.self,
                                                                    CocktailsPageViewController.self]
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override init(transitionStyle style: UIPageViewControllerTransitionStyle, navigationOrientation: UIPageViewControllerNavigationOrientation, options: [String : Any]?) {
        super.init(transitionStyle: style, navigationOrientation: navigationOrientation, options: options)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        insertOrdersVc()
    }
    
    //viewcontroller before
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let indexOfVc = pagedViewControllersTypes.index (where: { t in type(of: viewController) == t }) {
            if(indexOfVc == 0) {
                return nil
            }
            
            let prevVc = pageContentviewControllers[indexOfVc-1]
            return prevVc
        }
        
        return nil
    }
    
    //viewcontroller after
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let indexOfVc = pagedViewControllersTypes.index (where: { t in type(of: viewController) == t }) {
            let nextVc = pageContentviewControllers[indexOfVc+1]
            
            return nextVc
        }
        
        return nil
    }
    
    fileprivate func insertOrdersVc() {
        addViewControllerAsChild(ordersViewController, parentVc: self)
    }
}










