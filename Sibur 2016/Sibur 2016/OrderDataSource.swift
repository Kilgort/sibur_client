//  OrderDataSource.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/10/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.
//

import Foundation
import UIKit



class OrderDataSource: NSObject, UITableViewDataSource {
    var ordersData: [OrderDescription]
    
    init(ordersData: [OrderDescription]) {
        self.ordersData = ordersData
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: OrderCell.cellId, for: indexPath) as! OrderCell
        
        updateCellData(cell, idx: indexPath.row)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ordersData.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    fileprivate func updateCellData(_ orderCell: OrderCell, idx: Int) {
        let orderDescription = ordersData[idx]
        orderCell.itemIdx = idx
        orderCell.orderTitleLabel.text = "\(idx+1). \(orderDescription.orderTitle)"
        orderCell.orderCountLabel.text = "\(orderDescription.orderCount)"
    }
}
