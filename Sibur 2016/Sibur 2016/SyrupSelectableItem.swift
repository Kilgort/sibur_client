//
//  SyrupSelection.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/4/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.
//

import Foundation

extension Syrup : SelectableItem {
    public var title : String {
        get {
            return self.rawValue
        }
    }
    
    static let allValues: [SelectableItem] = [None, Caramel, Vanil, Chocolate, Walnut]
}