//
//  DeviceDto.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/22/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.
//

import Foundation

class DeviceDto {
    var id: String
    var number: Int
    var vendorId: String
    
    init(id: String, number: Int, vendorId: String) {
        self.id = id
        self.number = number
        self.vendorId = vendorId
    }
}