//
//  CoffeePageViewController.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 12/23/15.
//  Copyright © 2015 Oleg Tyshchenko. All rights reserved.
//

import UIKit

class CoffeePageViewController: SiburOrderViewController {
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet var checkButtonsCollection: [CheckButton]!
    @IBOutlet var syrupButtonsCollection: [NamedButton]!
    fileprivate var selectionPopover: SelectionPopover?
    
    init() {
        super.init(nibName: "CoffeePageViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        backgroundImage.image = coffeeBackgroundImage
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if(selectionPopover != nil) {
            selectionPopover = nil //to clear all memory here
        }
    }
    
    fileprivate func getSyrupForItem(_ itemName: String) -> Syrup {
        if let syrupBtnTitle = getSyrupButton(itemName)?.title(for: UIControlState()) {
            let s = Syrup(rawValue: syrupBtnTitle)
            return s == nil ? .None : s!
        }
        
        return .None
    }
    
    fileprivate func getIsChecked(_ itemName: String, checkType: String) -> Bool {
        if let isChecked = getCheckButton(itemName, checkType: checkType)?.isChecked {
            return isChecked
        }
        
        return false
    }
    
    fileprivate func getCheckButton(_ itemName: String, checkType: String) -> CheckButton? {
        let filteredCheckButtons = checkButtonsCollection.filter({cb in (cb.name.contains(itemName) && cb.name.contains(checkType)) })
        
        return filteredCheckButtons.first
    }
    
    fileprivate func getSyrupButton(_ itemName: String) -> NamedButton? {
        let filteredSyrupButtons = syrupButtonsCollection.filter({bb in bb.name.contains(itemName)})
        
        return filteredSyrupButtons.first
    }
    
    //NOTE: do I need to clean selections?
    fileprivate func clearSelections(_ itemName: String) {
        if let syrupButton = getSyrupButton(itemName) {
            syrupButton.setTitle(Syrup.None.rawValue, for: UIControlState())
        }
        
        if let milkCheck = getCheckButton(itemName, checkType: "milk") {
            milkCheck.isChecked = false
        }
        
        if let sugarCheck = getCheckButton(itemName, checkType: "sugar") {
            sugarCheck.isChecked = false
        }
    }
    
    @IBAction func ingridientsCheckAction(_ sender: CheckButton) {
        sender.isChecked = !(sender.isChecked)
        
        if(sender.isChecked) {
            sender.setTitleColor(checkboxCheckedTitlteColor, for: .normal)
        } else {
            sender.setTitleColor(checkboxUnccheckedTitlteColor, for: .normal)
        }
    }
    
    @IBAction func plusAction(_ sender: NamedButton) {
        let itemName = sender.name
        let coffeeItem = createCoffeeFromUiData(itemName)
        
        clearSelections(itemName)
        super.addItemToReceipt(coffeeItem)
    }
    
    fileprivate func createCoffeeFromUiData(_ itemName: String) -> Coffee {
        let syrup = getSyrupForItem(itemName)
        let isMilkChecked = getIsChecked(itemName, checkType: "milk")
        let isSugarChecked = getIsChecked(itemName, checkType: "sugar")
        
        return createCoffee(itemName, withSugar: isSugarChecked, withMilk: isMilkChecked, syrup: syrup)!
    }
    
    @IBAction func syrupSelectionAction(_ sender: BorderedButton) {
        selectionPopover = SelectionPopover(sourceView: sender, selectionItems: Syrup.allValues)
        
        selectionPopover?.selectionStyle =
            SelectionStyle(itemFont: nil, itemColor: UIColor.white, boxBackgroundColor: volumeTextColor, additionalWidth: 0)
        
        selectionPopover!.present(self) {
            si in sender.setTitle(si.title, for: UIControlState())
        }
    }
}
