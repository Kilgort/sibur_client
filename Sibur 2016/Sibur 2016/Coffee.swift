//
//  Coffee.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 12/22/15.
//  Copyright © 2015 Oleg Tyshchenko. All rights reserved.
//

import Foundation

open class Coffee : HotBeverage {
    open var withMilk: Bool
    open var syrup: Syrup
    
    init(name: String, withSugar: Bool, withMilk: Bool, syrup: Syrup) {
        self.withMilk = withMilk
        self.syrup = syrup
        super.init(name: name, withSugar: withSugar)
    }

//    required public init?(coder aDecoder: NSCoder) {
//        withMilk = aDecoder.decodeObjectForKey("withMilk") as! Bool
//        syrup = Syrup(rawValue: aDecoder.decodeObjectForKey("syrup") as! String)!
//        super.init(coder: aDecoder)
//    }
//    
//    override public func encodeWithCoder(aCoder: NSCoder) {
//        super.encodeWithCoder(aCoder)
//        aCoder.encodeObject(withMilk, forKey: "withMilk")
//        aCoder.encodeObject(syrup.rawValue, forKey: "syrup")
//    }
}
