//
//  AppDelegate.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 12/16/15.
//  Copyright © 2015 Oleg Tyshchenko. All rights reserved.
//

import UIKit
import CoreGraphics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var pageVc: UIPageViewController?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        cacheBackgroundImages()
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        setupRootViewController()
        window?.makeKeyAndVisible()
                
        return true
    }
    
    fileprivate func setupRootViewController() {
        self.window?.rootViewController = createAndInitPageVc()
    }
}
