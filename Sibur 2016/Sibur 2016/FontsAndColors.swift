//
//  Fonts.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 12/24/15.
//  Copyright © 2015 Oleg Tyshchenko. All rights reserved.

import Foundation
import UIKit

let ingridientTextColor = colorFromRgb(88, g: 89, b: 91, a: 255)
let volumeTextColor = colorFromRgb(19, g:122, b:128, a:255)

let orderButtonColor = colorFromRgb(159, g:146, b:193, a:255)
let orderReadyColor = colorFromRgb(21, g:169, b:158, a:255)

let itemVolumeFont = UIFont(name: "DINPro-Italic", size: 24)
let itemIngridientFont = UIFont(name: "DINPro-BlackItalic", size: 30)
let orderTitleFont = UIFont(name:"DINPro-Medium", size: 38)
let orderPositionFont = UIFont(name: "DINPro", size: 21)
let orderButtonFont = UIFont(name:"DINPro-Black", size: 16)
let orderStatusFont = UIFont(name:"DINPro-Black", size: 14)

let checkboxCheckedTitlteColor = colorFromRgb(30, g: 175, b: 178, a: 255)
let checkboxUnccheckedTitlteColor = colorFromRgb(163, g: 163, b: 163, a: 255)

private func colorFromRgb(_ r: Int, g: Int, b: Int, a: Int) -> UIColor {
    let red = CGFloat(r)/255.0
    let green = CGFloat(g)/255.0
    let blue = CGFloat(b)/255.0
    let alpha = CGFloat(a)/255.0
    
    return UIColor(red: red, green: green, blue: blue, alpha: alpha)
}
