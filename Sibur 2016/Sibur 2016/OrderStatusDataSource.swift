//  OrderStatusDataSource.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/11/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.
//

import Foundation
import UIKit

class OrderStatusDataSource: NSObject, UICollectionViewDataSource {
    var remoteReceiptsData: [RemoteReceiptDto] = []

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return remoteReceiptsData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let remoteOrderCell = collectionView
            .dequeueReusableCell(withReuseIdentifier: RemoteOrderCell.cellId, for: indexPath) as! RemoteOrderCell
        let remoteReceipt = remoteReceiptsData[indexPath.row]
        
        remoteOrderCell.setOrderStatus(parseOrderStatus(remoteReceipt.status))
        remoteOrderCell.setOrderItemNumber(remoteReceipt.number)
        
        return remoteOrderCell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    fileprivate func parseOrderStatus(_ orderStatus: String) -> OrderStatus {
        return orderStatus == "in progress" ? OrderStatus.inProcess : OrderStatus.ready
    }
}
