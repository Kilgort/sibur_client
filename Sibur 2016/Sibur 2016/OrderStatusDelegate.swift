//
//  OrderStatusDelegate.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/21/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.
//

import Foundation
import UIKit

class OrderStatusDelegate: NSObject, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 210, height: 44)
    }
}
