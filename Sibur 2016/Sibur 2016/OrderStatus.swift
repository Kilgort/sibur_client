//
//  OrdersStatus.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/10/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.
//

import Foundation

enum OrderStatus {
    case inProcess
    case ready
}
