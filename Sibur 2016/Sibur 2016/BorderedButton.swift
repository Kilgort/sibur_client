//
//  BorderedButton.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 12/27/15.
//  Copyright © 2015 Oleg Tyshchenko. All rights reserved.
//

import Foundation
import UIKit

class BorderConfig {
    var thickness: Float
    var radius: Float
    var color: UIColor
    
    init(thickness: Float, radius: Float, color: UIColor) {
        self.thickness = thickness
        self.radius = radius
        self.color = color
    }
}

@IBDesignable
class BorderedButton : NamedButton {
    @IBInspectable
    var borderThickness : NSDecimalNumber? {
        didSet {
            updateBorder()
        }
    }
    
    @IBInspectable
    var borderColor : UIColor? {
        didSet {
            updateBorder()
        }
    }
    
    @IBInspectable
    var borderRadius : NSDecimalNumber? {
        didSet {
            updateBorder()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        updateBorder()
    }
    
    func setBorder(_ borderConfig: BorderConfig) {
        borderThickness = NSDecimalNumber(value: borderConfig.thickness as Float)
        borderRadius = NSDecimalNumber(value: borderConfig.radius as Float)
        borderColor = borderConfig.color
    }
    
    fileprivate func updateBorder() {
        if(borderThickness != nil && borderColor != nil && borderRadius != nil) {
            self.layer.borderColor = (borderColor?.cgColor)!
            self.layer.borderWidth = CGFloat(borderThickness!)
            let cornerRad = CGFloat(borderRadius!)
            self.layer.cornerRadius = cornerRad
        }
    }
}
