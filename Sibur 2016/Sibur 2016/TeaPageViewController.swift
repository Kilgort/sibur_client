//
//  TeaPageViewController.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 12/23/15.
//  Copyright © 2015 Oleg Tyshchenko. All rights reserved.
//

import UIKit


//TODO: add check button control
class TeaPageViewController: SiburOrderViewController {
    @IBOutlet var sugarChecksCollection: [CheckButton]!
    
    @IBOutlet weak var backgroundImage: UIImageView!
    init() {
        super.init(nibName: "TeaPageViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundImage.image = teaBackgroundImage
        applyStyles()
    }
    
    fileprivate func applyStyles() {
        
    }
    
    fileprivate func getCheckButton(_ itemName: String) -> CheckButton? {
        let filteredCheckButtons = sugarChecksCollection.filter({cb in cb.name.contains(itemName)})
        
        return filteredCheckButtons.first
    }
    
    @IBAction func sugarCheckAction(_ sender: CheckButton) {
        sender.isChecked = !sender.isChecked
        
        if(sender.isChecked) {
            sender.setTitleColor(checkboxCheckedTitlteColor, for: .normal)
        } else {
            sender.setTitleColor(checkboxUnccheckedTitlteColor, for: .normal)
        }
    }
    
    @IBAction func teaPlusAction(_ sender: NamedButton) {
        let itemName = sender.name
        var isSugar = false
        
        if let checkBtn = getCheckButton(itemName) {
            isSugar = checkBtn.isChecked
            checkBtn.isChecked = false
        }
        
        if let tea = createTea(itemName, withSugar: isSugar) {
            super.addItemToReceipt(tea)
        }
    }
    
    @IBAction func makaroniPlusAction(_ sender: NamedButton) {
        let makaroni = Makaroni()
        super.addItemToReceipt(makaroni)
    }
}
