//
//  Makaroni.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/15/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.
//

import Foundation


open class Makaroni: SiburItem {
    init() {
        super.init(name: "Makaroni")
    }

//    required public init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//    }
    
    override var fullTitle: String {
        get {
            return "Макарони"
        }
        set {
            //do nothing
        }
    }
    
    
}
