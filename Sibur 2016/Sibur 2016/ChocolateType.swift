//
//  ChocolateType.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/20/17.
//  Copyright © 2017 Oleg Tyshchenko. All rights reserved.
//

import Foundation

public enum ChocolateType : String {
    case bitterCoco = "Горький 75% какао"
    case bitterAlmond = "Горький 75% с цукатами и миндалем"
    case milkCaco = "Молочный 42% какао"
    case milkHazelnut = "Молочный 42% с фундуком"
}

extension ChocolateType : SelectableItem {
    public var title : String {
        get {
            return self.rawValue
        }
    }
    
    static let allValues: [SelectableItem] = [bitterCoco, bitterAlmond, milkCaco, milkHazelnut]
}
