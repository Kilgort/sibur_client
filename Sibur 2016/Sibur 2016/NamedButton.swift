//
//  NamedButton.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/4/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.
//

import Foundation
import UIKit

open class NamedButton : UIButton {
    @IBInspectable var name: String = "default"
}
