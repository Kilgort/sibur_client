//
//  SiburViewExtension.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/4/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.
//

import Foundation
import UIKit

func addViewControllerAsChild(_ childVc: UIViewController, parentVc: UIViewController) {
    childVc.willMove(toParentViewController: parentVc)
    parentVc.addChildViewController(childVc)
    parentVc.view.addSubview(childVc.view)
    childVc.didMove(toParentViewController: parentVc)
}

func removeChildViewController(_ childVc: UIViewController) {
    childVc.willMove(toParentViewController: nil)
    childVc.removeFromParentViewController()
    childVc.view.removeFromSuperview()
    childVc.didMove(toParentViewController: nil)
}

func addCornersToView(_ view: UIView, cornerRadius: CGFloat) {
    view.layer.cornerRadius = cornerRadius
}

func addShadowToView(_ view: UIView, shadowsConfig: ShadowsConfig) {
    let vLayer = view.layer
    
    vLayer.shadowColor = shadowsConfig.color.cgColor
    vLayer.shadowOpacity = shadowsConfig.opacity
    vLayer.shadowRadius = CGFloat(shadowsConfig.radius)
    vLayer.shadowOffset = shadowsConfig.offset
}

func setPageViewControllerAsMainPage() {
    let pageVc = createAndInitPageVc()
    (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = pageVc
}

func setSetupViewControllerAsMainPage() {
    let setupVc = SetupViewController()
    (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = setupVc
}

func createResourceUrl(_ serverIp: String, resource: String) -> String {
    return "http://" + serverIp + ":8080" + "/" + resource
}

func createAndInitPageVc() -> UIPageViewController {
    let pageVc = MainPageViewController(transitionStyle: .scroll, navigationOrientation: .vertical, options: nil)
    let pageContentVc = [CoffeePageViewController()]
    pageVc.setViewControllers(pageContentVc, direction:  .forward, animated: true, completion: nil)
    
    return pageVc
}

func showSimpleAlert(_ message: String) {
    let alertView = UIAlertView(title: "Внимание", message: message, delegate: nil, cancelButtonTitle: nil, otherButtonTitles: "OK")
    
    alertView.show()
}


private let defaultServerPort = "8080"

func isServerReachableAsync(_ serverUrl: String, onReach: @escaping (Bool) -> ()) {
    let pingUrl = "http://" + serverUrl + ":" + defaultServerPort + "/" + "ping"
    let request = createIsReachableRequest(pingUrl)
    
    NSURLConnection.sendAsynchronousRequest(request, queue: OperationQueue.main, completionHandler:
        {
            (resp, data, error) in
            if error != nil {
                onReach(false)
            }
            else {
                if let d = data {
                    let resultString = NSString(data: d, encoding: String.Encoding.utf8.rawValue)
                    
                    if resultString!.contains("pong") {
                        onReach(true)
                    }
                }
                else {
                    onReach(false)
                }
            }
    })
}

private func createIsReachableRequest(_ url: String) -> URLRequest {
    let request = NSMutableURLRequest(url: URL(string: url)!)
    request.timeoutInterval = 5
    request.httpMethod = "GET"
    
    return request as URLRequest
}



