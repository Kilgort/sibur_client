//
//  CocktailsPageViewController.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 12/22/15.
//  Copyright © 2015 Oleg Tyshchenko. All rights reserved.
//

import UIKit


//NOTE: this is page controller for drinks
class CocktailsPageViewController: SiburOrderViewController {
    fileprivate var cocktailInfoPopopverController: InfoViewController?
    
    @IBOutlet weak var alcoCheckButton: CheckButton!
    @IBOutlet weak var noAlcoCheckButton: CheckButton!
    
    @IBOutlet weak var sparkyCheckButton: CheckButton!
    @IBOutlet weak var stillCheckButton: CheckButton!
    
    @IBOutlet weak var backgroundImage: UIImageView!
    
    init() {
        super.init(nibName: "CocktailsPageViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupRadioBoxButtonsState()
        backgroundImage.image = cocktailsBackgroundImage
    }
    
    fileprivate func setupRadioBoxButtonsState() {
        checkRadioButton(checkButton: alcoCheckButton, isChecked: true)
        checkRadioButton(checkButton: noAlcoCheckButton, isChecked: false)
        
        checkRadioButton(checkButton: sparkyCheckButton, isChecked: true)
        checkRadioButton(checkButton: stillCheckButton, isChecked: false)
    }
    
    fileprivate func isMandarinAlco() -> Bool {
        return alcoCheckButton.isChecked
    }
    
    fileprivate func isWaterSparky() -> Bool {
        return sparkyCheckButton.isChecked
    }
    
    fileprivate func checkRadioButton(checkButton : CheckButton, isChecked: Bool) {
        checkButton.isChecked = isChecked
        
        if(isChecked) {
            checkButton.setTitleColor(checkboxCheckedTitlteColor, for: .normal)
        } else {
            checkButton.setTitleColor(checkboxUnccheckedTitlteColor, for: .normal)
        }
    }
    
    fileprivate func getDrinkAdditionalTitle(_ itemName: String) -> String? {
        if itemName == "water" {
            return isWaterSparky() ? " газированная" : " без газа"
        }
        
        if itemName == "mandarin" {
            return isMandarinAlco() ? " алкогольный" : " безалкогольный"
        }
        
        
        return nil
    }
    
    @IBAction func mandarinAlcoCheckAction(_ sender: CheckButton) {
        let checkName = sender.name
        
        if !sender.isChecked {
            checkRadioButton(checkButton: sender, isChecked: true)
            
            if checkName == "mandarin_alco" {
                checkRadioButton(checkButton: noAlcoCheckButton, isChecked: false)
            } else {
                checkRadioButton(checkButton: alcoCheckButton, isChecked: false)
            }
        }
    }
    
    @IBAction func waterCheckAction(_ sender: CheckButton) {
        let checkName = sender.name
        
        if !sender.isChecked {
            checkRadioButton(checkButton: sender, isChecked: true)
            
            if checkName == "water_sparky" {
                checkRadioButton(checkButton: stillCheckButton, isChecked: false)
            } else {
                checkRadioButton(checkButton: sparkyCheckButton, isChecked: false)
            }
        }
    }
    
    @IBAction func plusAction(_ sender: NamedButton) {
        let itemName = sender.name
        
        if let cocktail = createDrink(itemName, additionaDrinkTitle: getDrinkAdditionalTitle(itemName)) {
            super.addItemToReceipt(cocktail!)
        }
    }
}
