//
//  Beer.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/3/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.
//

import Foundation


open class Beer : Alcohol {
    open var beerType : BeerType
    
    init(name: String, beerType: BeerType) {
        self.beerType = beerType
        super.init(name: name)
    }
//
//    required public init?(coder aDecoder: NSCoder) {
//        beerType = BeerType(rawValue: aDecoder.decodeObjectForKey("beerType") as! Int)!
//        super.init(coder: aDecoder)
//    }
//    
//    override public func encodeWithCoder(aCoder: NSCoder) {
//        super.encodeWithCoder(aCoder)
//        aCoder.encodeObject(beerType.rawValue, forKey: "beerType")
//    }
}
