//
//  Pie.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/20/17.
//  Copyright © 2017 Oleg Tyshchenko. All rights reserved.
//

import Foundation

public enum Pie : String {
    case Meat = "Мясо"
    case Cabbage = "Капуста"
    case Apple = "Яблоко"
}

extension Pie : SelectableItem {
    public var title : String {
        get {
            return self.rawValue
        }
    }
    
    static let allValues: [SelectableItem] = [Meat, Cabbage, Apple]
}
