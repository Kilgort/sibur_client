//
//  SetupViewController.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/21/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.
//

import UIKit
import Alamofire


class SetupViewController: UIViewController {
    @IBOutlet weak var serverCheckButton: UIButton!
    @IBOutlet weak var serverAddressTextField: UITextField!
    @IBOutlet weak var serverCheckIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var saveAndContinueButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if SetupDataService.isDeviceRegisteredLocally() {
            serverAddressTextField.text = SetupDataService.getServerIp()
            
            isServerReachableAsync(SetupDataService.getServerIp()) {
                isReachable in
                
                if isReachable {
                    SetupDataService.isDeviceRegistered {
                        isRegistered in
                        
                        if isRegistered {
                            self.setPageViewControllerAsMainPage()
                        }
                        else {
                            showSimpleAlert("Не удалось подключиться к серверу. Сервер не распознал данное устройство, обратитесь к администратору или зарегестрируйтесь еще раз.")
                        }
                    }
                }
                else {
                    showSimpleAlert("Не удалось подключиться к серверу. Проверьте правильность адреса и попробуйте еще раз")
                }
            }
            
        }
        
        serverCheckIndicator.startAnimating()
        serverCheckIndicator.isHidden = true
    }

    func setPageViewControllerAsMainPage() {
        let pageVc = createAndInitPageVc()
        (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = pageVc
    }
    
    @IBAction func checkServerAction(_ sender: UIButton) {
        doCheckServerAddress()
    }
    
    @IBAction func saveSettingsAction(_ sender: UIButton) {
        
        if isServerAddressValid(serverAddressTextField.text) {
            startServerCheckingIndication()
            
            isServerReachableAsync(serverAddressTextField.text!) {
                isReachable in
                self.stopServerCheckingIndication()
                
                if isReachable {
                    self.saveSettingsAndNavigateFurther()
                }
                else {
                    showSimpleAlert("Не получилось подключиться к данному адресу сервера, обратитесь к администратору за правильным адресом и попробуйте еще раз")
                }
            }
        }
        else {
            showAlertIfServerAddressIsNotValid()
        }
    }
    
    fileprivate func saveSettingsAndNavigateFurther() {
        SetupDataService.isDeviceRegistered {
            isRegistered in
            
            if(isRegistered) {
                SetupDataService.saveServerIp(self.serverAddressTextField.text!)
                
                if self.presentingViewController != nil {
                    self.dismiss(animated: true, completion: nil)
                }
                else {
                    self.setPageViewControllerAsMainPage()
                }
            } else {
                SetupDataService.saveServerIp(self.serverAddressTextField.text!)
                self.registerDeviceAndNavigateToMainPage()
            }
        }
        
    }
    
    fileprivate func registerDeviceAndNavigateToMainPage() {
        let registrationUrl = createResourceUrl(SetupDataService.getServerIp(), resource: "register_device")
        let vendorId = UIDevice.current.identifierForVendor?.uuidString
        
        Alamofire.request(registrationUrl, method: .post, parameters: vendorIdParameters(vendorId!), encoding: JSONEncoding.default)
            .responseJSON {
                response in
                if let json = response.result.value {
                    let jsonDict = json as! Dictionary<NSObject, AnyObject>
                    let deviceInfo = mapJsonToDeviceInfo(jsonDict)
                    
                    SetupDataService.saveDeviceRegistration(deviceInfo)
                    self.setPageViewControllerAsMainPage()
                }
                else {
                    showSimpleAlert("Произошла ошибка на этапе регистрации устройства в системе. Обратитесь к администратору или повторите попытку позже")
                }
        }
    }
    
    fileprivate func doCheckServerAddress() {
        
        if isServerAddressValid(serverAddressTextField.text!) {
            startServerCheckingIndication()
            
            isServerReachableAsync(serverAddressTextField.text!) {
                isReachable in
                
                self.stopServerCheckingIndication()
                
                if isReachable {
                    showSimpleAlert("Подключение к серверу успешно")
                }
                else {
                    showSimpleAlert("Не получилось подключиться к данному адресу сервера, обратитесь к администратору за правильным адресом и попробуйте еще раз")
                }
            }
            
        }
        else {
            showAlertIfServerAddressIsNotValid()
        }
    }
    
    fileprivate func startServerCheckingIndication() {
        serverCheckIndicator.isHidden = false
        saveAndContinueButton.isEnabled = false
        serverCheckButton.isEnabled = false
    }
    
    
    fileprivate func stopServerCheckingIndication() {
        serverCheckIndicator.isHidden = true
        saveAndContinueButton.isEnabled = true
        serverCheckButton.isEnabled = true
    }
    
    fileprivate func isServerAddressValid(_ serverAddress: String?) -> Bool {
        if serverAddress == nil {
            return false
        }
        
        let ipAddressPredicate = NSPredicate(format: "SELF MATCHES %@", "^(?:[0-9]{1,3}\\.){3}[0-9]{1,3}$")
        return ipAddressPredicate.evaluate(with: serverAddress)
    }
    
    fileprivate func showAlertIfServerAddressIsNotValid() {
        showSimpleAlert("Вы ввели неверный адрес сервера, попробуйте еще раз")
    }
}
