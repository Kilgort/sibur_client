//
//  CocktailsFactory.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/13/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.
//

import Foundation

class CocktailNames {
    static let happiness = "happiness"
    static let joy = "joy"
    static let interest = "interest"
    static let pleasure = "pleasure"
}

class BeerNames {
    static let darkBeer = "serenity_black"
    static let lightBeer = "serenity_white"
}

class CoffeeNames {
    static let cappucino = "cappucino"
    static let espresso = "espresso"
    static let latte = "latte"
    static let americano = "americano"
    static let coffee = "coffee"
}

class TeaNames {
    static let blackTea = "black_classic"
    static let greenTea = "green_classic"
    static let cranberry = "cranberry"
    static let buckthorn = "buckthorn"
    static let ginger = "ginger"
}

class ChocolateNames {
    static let milkChocolate = "choco_milk"
    static let lemonChocolate = "choco_lemon"
    static let blackChocolate = "choco_black"
    static let orangeChocolate = "choco_orange"
    static let hazelnutChocolate = "choco_hazelnut"
}

class SnackNames {
    static let roastBeef = "roast_beef"
    static let parmPouch = "parm_pouch"
    static let shrimp = "shrimp"
    static let verin = "verin"
    static let fruit = "fruit"
    static let caviar = "caviar"
}

let drinksNamesTitlesDictionary: [String: String] =
[
    "mandarin" : "Мандариновый фреш",
    "margarita" : "Клубничная Маргарита с дымом",
    "tricolor" : "Шот Россия Триколор",
    "whiskey" : "Коктейль Виски Сауэр",
    "lemonade" : "Клубничный лимонад",
    "water" : "Минеральная вода Акваника"
]

let coffeeNamesBaseTitlesDictionary: [String: String] =
[
    CoffeeNames.cappucino: "Капучино",
    CoffeeNames.espresso: "Эспрессо",
    CoffeeNames.latte: "Латтэ",
    CoffeeNames.americano: "Американо",
    CoffeeNames.coffee: "РАФ кофе"
]

let teaNamesBaseTitlesDictionary: [String: String] =
[
    TeaNames.blackTea: "Чай черный",
    TeaNames.greenTea: "Чай зеленый",
    TeaNames.cranberry: "Чай клюквенный",
    TeaNames.ginger: "Чай имбирный",
    TeaNames.buckthorn: "Чай облепиховый"
]

let chocolateNamesTitlesDictionary: [String: String] =
[
    "opera" : "Опера",
    "potato" : "Картошка в глазуре",
    "mango" : "Мусс с манго и физалисом",
    "cream" : "Мини пирожное с кремом Ганаш и малиной",
    "panakota" : "Панакота",
    "choco" : "Шоколад"
]

let snacksNamesTitlesDictionary: [String : String] =
[
    SnackNames.roastBeef : "Ростбиф с рукколой на прищепке",
    SnackNames.parmPouch : "Пармский мешочек",
    SnackNames.shrimp : "Креветка на шпажке в шоте с кисло-сладким соусом",
    SnackNames.verin : "Итальянский Верин",
    SnackNames.fruit : "Фруктовая шпажка",
    SnackNames.caviar : "Шу с икрой"
]

let foodNameTitlesDictionary : [String : String] =
[
    "sandwich_parma" : "Сэндвич с Пармой, моцареллой и рукколой",
    "sandwich_salmon" : "Сэндвич с лососем и салатом",
    "roll_herring" : "Ролл в лаваше Сельд под шубой",
    "roll_ceaser" : "Ролл Цезарь",
    "pie" : "Пирожки ассорти"
]

let pieTypeTitleDictionary : [Pie : String] =
[
    Pie.Apple : " с яблоком",
    Pie.Meat : " с мясом",
    Pie.Cabbage : " с капустой"
]

func createDrink(_ drinkName: String, additionaDrinkTitle: String?) -> SiburItem?? {
    if let drinkTitle = drinksNamesTitlesDictionary[drinkName] {

        let drink = SiburItem(name: drinkName)
        drink.fullTitle = drinkTitle
        
        if(additionaDrinkTitle != nil) {
            drink.fullTitle += additionaDrinkTitle!
        }
        
        return drink
    }
    
    return nil
}

func createCoffee(_ name: String, withSugar: Bool, withMilk: Bool, syrup: Syrup) -> Coffee? {
    if let coffeeTitle = produceCoffeeTitle(name, withSugar: withSugar, withMilk: withMilk, syrup: syrup) {
        let coffee = Coffee(name: name, withSugar: withSugar, withMilk: withMilk, syrup: syrup)
        
        coffee.fullTitle = coffeeTitle
        return coffee
    }
    
    return nil
}

func createSnack(_ name: String) -> SiburItem? {
    if let snackTitle = snacksNamesTitlesDictionary[name] {
        let snackItem = SiburItem(name: name)
        snackItem.fullTitle = snackTitle
        
        return snackItem
    }
    
    return nil
}

func createFood(_ name: String, pieIngridient : Pie?) -> SiburItem? {
    if let foodTitle = foodNameTitlesDictionary[name] {
        let foodItem = SiburItem(name: name)
        
        foodItem.fullTitle = foodTitle
        
        if(pieIngridient != nil) {
            foodItem.fullTitle += pieTypeTitleDictionary[pieIngridient!]!
        }
        
        return foodItem
    }
    
    return nil
}

func createTea(_ name: String, withSugar: Bool) -> Tea? {
    if let teaTitle = produceTeaTitle(name, withSugar: withSugar) {
        let tea = Tea(name: name, withSugar: withSugar)
        tea.fullTitle = teaTitle

        return tea
    }
    
    return nil
}

func createChocolate(_ name: String, chocolateType: ChocolateType?) -> Chocolate? {
    if let chocoTitle = chocolateNamesTitlesDictionary[name] {
        let chocolate = Chocolate(name: name)
        chocolate.fullTitle = chocoTitle
        
        if let chocoTypeTitle = chocolateType?.rawValue {
            chocolate.fullTitle += " с " + chocoTypeTitle
        }
        
        return chocolate
    }
    
    return nil
}

private func produceCoffeeTitle(_ name: String, withSugar: Bool, withMilk: Bool, syrup: Syrup) -> String? {
    if let baseCoffeeTitle = coffeeNamesBaseTitlesDictionary[name] {
        let syrupAppendedTitle = appendSyrupToCoffee(baseCoffeeTitle, syrup: syrup)
        let milkAppendedTitle = appendMilkToCoffee(syrupAppendedTitle, withMilk: withMilk)
        let sugarAppendedTitle = appendSugarToCoffee(milkAppendedTitle, withSugar: withSugar)
        
        return sugarAppendedTitle
    }
    
    return nil
}

private func appendSyrupToCoffee(_ coffeeTitle: String, syrup: Syrup) -> String {
    if syrup != .None {
        let syrupTitle = coffeeTitle + " с " + produceSyrupTitle(syrup)
        return syrupTitle
    }
    
    return coffeeTitle
}

private func appendMilkToCoffee(_ coffeeTitle: String, withMilk: Bool) -> String {
    if withMilk {
        let milkBaseTitle = "молоком"
        let milkPrefix = coffeeTitle.contains(" с ") ? " и " : " с "
        let milkTitle = coffeeTitle + milkPrefix + milkBaseTitle
        
        return milkTitle
    }
    
    return coffeeTitle
}

private func appendSugarToCoffee(_ coffeeTitle: String, withSugar: Bool) -> String {
    if withSugar {
        let sugarBaseTitle = "сахаром"
        
        //Simple Case:
        if !(coffeeTitle.contains(" с ") || coffeeTitle.contains(" и ")) {
            return coffeeTitle + " с " + sugarBaseTitle
        }
        
        //Complex case:
        if coffeeTitle.contains(" с ") && coffeeTitle.contains(" и ") {
            let andSwappedToCommaTitle = coffeeTitle.replacingOccurrences(of: " и ", with: ", ")
            return andSwappedToCommaTitle + " и " + sugarBaseTitle
        }
        
        //General case:
        return coffeeTitle + " и " + sugarBaseTitle
    }
    
    return coffeeTitle
}

private func produceTeaTitle(_ name: String, withSugar: Bool) -> String? {
    if let baseTeaTitle = teaNamesBaseTitlesDictionary[name] {
        return appendSugarToTea(baseTeaTitle, withSugar: withSugar)
    }
    
    return nil
}

private func appendSugarToTea(_ teaTitle: String, withSugar: Bool) -> String {
    if withSugar {
        let sugarTitle = "сахаром"
        let prefix = teaTitle.contains(" с ") ? " и " : " с "
        let resultTitle = teaTitle + prefix + sugarTitle
        
        return resultTitle
    }
    
    return teaTitle
}

private func produceSyrupTitle(_ syrup: Syrup) -> String {
    switch syrup {
    case .Caramel:
        return "карамельным сиропом"
    case .Chocolate:
        return "шоколадным сиропом"
    case .Vanil:
        return "ванильным сиропом"
    case .Walnut:
        return "ореховым сиропом"
    default:
        return ""
    }
}

private func parseBeerType(_ beerName: String) -> BeerType {
    return beerName.contains("white") ? .light : .dark
}

private func beerNameIsCorrect(_ beerName: String) -> Bool {
    return beerName.contains("white") || beerName.contains("black")
}
