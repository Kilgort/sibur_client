//
//  ChocolatePageViewController.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 12/23/15.
//  Copyright © 2015 Oleg Tyshchenko. All rights reserved.
//

import UIKit

class ChocolatePageViewController: SiburOrderViewController {
    fileprivate var selectionPopover: SelectionPopover?
    
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var chocoSelectionButton: NamedButton!
    
    
    init() {
        super.init(nibName: "ChocolatePageViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundImage.image = chocolateBackgroundImage
    }
    
    @IBAction func chocoSelectionAction(_ sender: NamedButton) {
        selectionPopover = SelectionPopover(sourceView: sender, selectionItems: ChocolateType.allValues)
        
        selectionPopover?.selectionStyle =
            SelectionStyle(itemFont: nil, itemColor: UIColor.white, boxBackgroundColor: volumeTextColor, additionalWidth: 150)
        
        selectionPopover!.present(self) {
            si in sender.setTitle(si.title, for: UIControlState())

        }
    }
   
    @IBAction func plusAction(_ sender: NamedButton) {
        let itemName = sender.name
        var chocolateType : ChocolateType? = nil
        
        if(itemName == "choco") {
            let chocoSelectionTitle = chocoSelectionButton.title(for: .normal)
            chocolateType = ChocolateType(rawValue: chocoSelectionTitle!)
        }
        
        if let chocolateItem = createChocolate(sender.name, chocolateType: chocolateType) {
            super.addItemToReceipt(chocolateItem)
        }
    }
}
