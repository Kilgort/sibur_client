//
//  RemoteReceiptTracker.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/21/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.
//

import Foundation

class RemoteReceiptTracker {
    fileprivate let receiptsUpdateDelaySeconds = 20.0

    fileprivate var schedulerTimer: Timer?
    fileprivate var receiptUpdateEventHandlers: [ObjectIdentifier: ([RemoteReceiptDto]) -> ()] = Dictionary()
    
    fileprivate init() {
        //start tracking
    }
    
    static var sharedInstance: RemoteReceiptTracker {
        struct Static {

            static let instance = RemoteReceiptTracker()
        }
        
        return Static.instance
    }
    
    //load all receipts once in a while
    
    //gives ability for client code to track receipts upated (register update events)
    
    var isTracking: Bool = false
    
    fileprivate var lastDownloadedReceipts: [RemoteReceiptDto] = []

    func subscribeForRemoteReceiptUpdates(_ target: AnyObject, onReceiptsUpdate: @escaping ([RemoteReceiptDto]) -> ()) {
        if !isTargetSubscribed(target) {
            let objId = ObjectIdentifier(target)
            receiptUpdateEventHandlers[objId] = onReceiptsUpdate
        }
        
        if receiptUpdateEventHandlers.count > 0 {
//            startTracking()
        }
    }
    
    func unSubscribeFromRemoteReceiptUpdates(_ target: AnyObject) {
        if isTargetSubscribed(target) {
            let objId = ObjectIdentifier(target)
            receiptUpdateEventHandlers.removeValue(forKey: objId)
        }
        
        if receiptUpdateEventHandlers.count < 1 {
            stopTracking()
        }
    }
    
    //start only if more than 1 subscriber
    func startTracking() {
        schedulerTimer =
            Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(RemoteReceiptTracker.timerUpdate(_:)), userInfo: nil, repeats: true)
        isTracking = true
    }
    
    //stop if no subscribers
    func stopTracking() {
        schedulerTimer?.invalidate()
        isTracking = false
        
        schedulerTimer = nil
    }
    
    @objc func timerUpdate(_ timer: Timer) {
        if timer == schedulerTimer { //code that is outside won't be able to call it
            updateRemoteReceipts(false)
        }
    }
    
    func updateRemoteReceipts(_ manually: Bool) {
        if manually {
            stopTracking() //doing this so timer won't start updating by itself causing potential race condition
        }
        
//        isServerReachableAsync(SetupDataService.getServerIp()) {
//            isReachable in
//            if isReachable {
//                ReceiptRemoteService.getReceipts {
//                    remoteReceipts in
//                    
//                    if let rr = remoteReceipts {
//                        self.lastDownloadedReceipts = rr
//                        self.raiseReceiptUpdatedEvent(rr)
//                    }
//                    
//                    if manually && self.receiptUpdateEventHandlers.count > 0 {
//                        self.startTracking()
//                    }
//                }
//            }
//            else {
//                self.stopTracking()
//                showSimpleAlert("Пропала связь с сервером. Проверьте подключение и адрес сервера и попробуйте еще раз.")
//                setSetupViewControllerAsMainPage()
//            }
//        }
    }
    
    fileprivate func isTargetSubscribed(_ target: AnyObject) -> Bool {
        let objId = ObjectIdentifier(target)
        
        return receiptUpdateEventHandlers.keys.contains(objId)
    }
    
    fileprivate func raiseReceiptUpdatedEvent(_ remoteReceiptDtos: [RemoteReceiptDto]) {
        for (_, ev) in receiptUpdateEventHandlers {
            ev(remoteReceiptDtos)
        }
    }
}
