//
//  OrderCell.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/8/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.
//

import UIKit

class OrderCell: UITableViewCell {
    static let cellNib = UINib(nibName: "OrderCell", bundle: Bundle.main)
    static let cellId = "OrderCell"
    
    @IBOutlet weak var orderTitleLabel: UILabel!
    @IBOutlet weak var orderCountLabel: UILabel!
    
    var itemIdx: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        orderTitleLabel.font = orderPositionFont
        orderCountLabel.font = orderPositionFont
    }
    
    @IBAction func orderMinusAction(_ sender: AnyObject) {
        let receiptService = ReceiptService.sharedInstance
        receiptService.decrementItemAtIndex(itemIdx!)
    }

    @IBAction func orderPlusAction(_ sender: AnyObject) {
        let receiptService = ReceiptService.sharedInstance
        receiptService.incrementItemAtIndex(itemIdx!)
    }
}
