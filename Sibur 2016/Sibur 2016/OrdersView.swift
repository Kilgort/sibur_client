//
//  OrdersView.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 12/28/15.
//  Copyright © 2015 Oleg Tyshchenko. All rights reserved.
//

import UIKit

class OrdersView: UIView {
    fileprivate static let viewName = "OrdersView"
    
    @IBOutlet weak var binButton: UIButton!
    
    static func createOrdersView() -> OrdersView {
        let ordersView = Bundle.main.loadNibNamed(viewName, owner: nil, options: nil)?[0] as! OrdersView
        
        return ordersView
    }
}
