//
//  SelectionData.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/4/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.
//

import Foundation
import UIKit

class SelectionTableDataSource : NSObject, UITableViewDataSource {
    static let selectionCellId = "SelectionCell"

    fileprivate let data: [SelectableItem]
    
    var cellTextFont: UIFont?
    var cellTextColor: UIColor?
    var cellBackgroundColor: UIColor?
    
    init(data: [SelectableItem]) {
        self.data = data
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SelectionTableDataSource.selectionCellId)!
        
        cell.textLabel?.text = data[indexPath.row].title
        setCellStyle(cell)
        
        return cell
    }
    
    fileprivate func setCellStyle(_ cell: UITableViewCell) {
        if let font = cellTextFont {
            cell.textLabel?.font = font
        }
        
        if let color = cellTextColor {
            cell.textLabel?.textColor = color
        }
        
        if let backColor = cellBackgroundColor {
            cell.backgroundColor = backColor
        }
    }
}

class SelectionTableDelegate : NSObject, UITableViewDelegate {
    var onRowSelectAction : ((Int) -> ())?
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectTitleFromPopup(tableView, indexPath: indexPath)
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    fileprivate func selectTitleFromPopup(_ tableView: UITableView, indexPath: IndexPath)  {
        if(onRowSelectAction != nil) {
            onRowSelectAction!(indexPath.row)
        }
    }
}


