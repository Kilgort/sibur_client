//
//  Styles.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 12/24/15.
//  Copyright © 2015 Oleg Tyshchenko. All rights reserved.
//

import Foundation
import UIKit

func applyVolumeStyle(_ label: UILabel) {
    label.font = itemVolumeFont
    label.textColor = volumeTextColor
}

func applyIngridientStyle(_ label: UILabel) {
    label.font = itemIngridientFont
    label.textColor = ingridientTextColor
}

func applyCounterTextStyle(_ label: UILabel, font: UIFont) {
    label.font = font
    label.textColor = ingridientTextColor
}
