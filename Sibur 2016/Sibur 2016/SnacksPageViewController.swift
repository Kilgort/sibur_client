//
//  SnacksPageViewController.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/20/17.
//  Copyright © 2017 Oleg Tyshchenko. All rights reserved.
//

import UIKit

class SnacksPageViewController: SiburOrderViewController {
    init() {
        super.init(nibName: "SnacksPageViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBAction func onPlusAction(_ sender: NamedButton) {
        let itemName = sender.name
        
        if let snackItem = createSnack(itemName) {
            super.addItemToReceipt(snackItem)
        }
    }
}
