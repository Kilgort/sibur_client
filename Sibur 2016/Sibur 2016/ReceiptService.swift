//  OrdersService.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/15/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.
//

import Foundation

let savedItemNames: String = "saved_items"

enum ReceiptPushStatus {
    case success
    case error(String?)
}

//Service that is responsible for collecting orders and pushing Receipt to server for bar tracking
class ReceiptService {
    fileprivate init() {
        //load receipt from local storage if needed
    }
    
    static var sharedInstance: ReceiptService {
        struct Static {
            static let instance = ReceiptService()
        }
        
        return Static.instance
    }
    
    fileprivate var receiptUpdateEventHandlers: [ObjectIdentifier: ([OrderDescription]) -> ()] = Dictionary()
    fileprivate var receiptInProgress: Receipt = Receipt()
    
    //public api
    
    var ordersDescriptions: [OrderDescription] {
        get {
            return receiptInProgress.ordersDescription
        }
    }
    
    func addItemToReceipt(_ item: SiburItem) {
        let order = Order(item: item)
        receiptInProgress.addOrder(order)
        
        raiseReceiptUpdatedEvent()
    }
    
    func incrementItemAtIndex(_ itemIndex: Int) {
        receiptInProgress.incrementOrder(itemIndex)
        
        raiseReceiptUpdatedEvent()
    }
    
    func decrementItemAtIndex(_ itemIndex: Int) {
        receiptInProgress.decrementOrder(itemIndex)
        
        raiseReceiptUpdatedEvent()
    }
    
    func indexOfItemInReceipt(_ itemName: String) -> Int {
        if let idxOfItem = receiptInProgress.orders.index(where: {rc in rc.orderName == itemName}) {
            return idxOfItem
        }
        
        return -1
    }
    
    func subscribeForReceiptUpdates(_ target: AnyObject, onReceiptUpdate: @escaping ([OrderDescription]) -> ()) {
        if !isTargetSubscribed(target) {
            let objId = ObjectIdentifier(target)
            receiptUpdateEventHandlers[objId] = onReceiptUpdate
        }
    }
    
    func unSubscribeFromReceiptUpdates(_ target: AnyObject) {
        if isTargetSubscribed(target) {
            let objId = ObjectIdentifier(target)
            receiptUpdateEventHandlers.removeValue(forKey: objId)
        }
    }
    
    func preserveReceipt() {
        //save to local storage state of receipt
        let orders = receiptInProgress.orders
        var ordersInfoDict = Dictionary<String, Int>()
        
        for order in orders {
            ordersInfoDict[order.orderItem.name] = order.orderCount
        }
    }
    
    //Pushes receipt to server for bar
    func pushReceipt(_ onPush: @escaping (ReceiptPushStatus) -> ()) {
        if receiptInProgress.orders.count < 1 {
            onPush(ReceiptPushStatus.error("Добавьте что нибудь в заказ."))
            return
        }
        isServerReachableAsync(SetupDataService.getServerIp()) {
            isReachable in
            
            if isReachable {
                ReceiptRemoteService.postReceipt(self.receiptInProgress) {
                    success in
                    
                    if !success {
                        onPush(ReceiptPushStatus.error(nil))
                    }
                    else {
                        onPush(ReceiptPushStatus.success)
                        self.clearOrders()
                    }
                }
            }
            else {
                showSimpleAlert("Пропала связь с сервером. Проверьте подключение и адрес сервера и попробуйте еще раз.")
                RemoteReceiptTracker.sharedInstance.stopTracking()
            }
        }

        
    }
    
    func clearOrders() {
        receiptInProgress = Receipt()
        raiseReceiptUpdatedEvent()
    }
    
    fileprivate func isTargetSubscribed(_ target: AnyObject) -> Bool {
        let objId = ObjectIdentifier(target)
        
        return receiptUpdateEventHandlers.keys.contains(objId)
    }
    
    fileprivate func raiseReceiptUpdatedEvent() {
        for (_, ev) in receiptUpdateEventHandlers {
            ev(receiptInProgress.ordersDescription)
        }
    }
    
    //how to track remotre orders?
}
