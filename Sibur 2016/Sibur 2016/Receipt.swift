//
//  Receipt.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/12/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.
//

import Foundation

struct OrderDescription {
    let orderTitle: String
    let orderCount: Int
    
    init(orderTitle: String, orderCount: Int) {
        self.orderTitle = orderTitle
        self.orderCount = orderCount
    }
}

//Contains collection of orders which will be send to server
class Receipt {
    var orders: [Order] = []

    var ordersDescription: [OrderDescription] {
        get {
            return orders.map {
                order in OrderDescription(orderTitle: order.orderTitle, orderCount: order.orderCount)
            }
        }
    }
    
    func addOrder(_ order: Order) {
        if orderIsDuplicate(order) {
            
            if let orderIdx = orders.index (where: { ord in ord.orderTitle == order.orderTitle }) {
                incrementOrder(orderIdx)
            }
            
            
        } else {
            order.orderCount = 1 //setting orderCount to 1 as initial
            orders.append(order)
        }
    }
    
    func incrementOrder(_ orderIdx: Int) {
        let order = orders[orderIdx]
        order.orderCount += 1
    }
    
    func decrementOrder(_ orderIdx: Int) {
        let order = orders[orderIdx]
        order.orderCount -= 1
        
        deleteOrderIfZeroCount(order, orderIdx: orderIdx)
    }
    
    fileprivate func orderIsDuplicate(_ order: Order) -> Bool {
        return orders.contains { ord in ord.orderTitle == order.orderTitle }
    }
    
    fileprivate func deleteOrderIfZeroCount(_ order: Order, orderIdx: Int) {
        if order.orderCount == 0 {
            orders.remove(at: orderIdx)
        }
    }
}

