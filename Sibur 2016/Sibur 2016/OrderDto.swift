//
//  OrderDto.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/20/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.
//

import Foundation


class OrderDto {
    var name: String
    var orderCount: Int
    var fullTitle: String
    
    init(name: String, orderCount: Int, fullTitle: String) {
        self.name = name
        self.orderCount = orderCount
        self.fullTitle = fullTitle
    }
}