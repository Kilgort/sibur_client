//
//  DataMappings.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/20/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.
//

import Foundation


func mapJsonToRemoteReceipt(_ receiptDict: Dictionary<NSObject, AnyObject>) -> RemoteReceiptDto {
    let id = receiptDict["id" as NSObject] as! String
    let status = receiptDict["status" as NSObject] as! String
    let createdDate = receiptDict["created" as NSObject] as! String
    let number = receiptDict["number" as NSObject] as! Int
    let ordersArray = receiptDict["orders" as NSObject] as! NSArray
    
    let orders = ordersArray.map { ordersJson in mapJsonToOrderReceipt(ordersJson as! Dictionary<NSObject, AnyObject>) }
    let remoteReceipt = RemoteReceiptDto(id: id, created: parseRemoteDateFromString(createdDate), number: number, status: status, orders: orders)
    
    return remoteReceipt
}

func mapJsonToOrderReceipt(_ orderJsonDict: Dictionary<NSObject, AnyObject>) -> OrderDto {
    let name = orderJsonDict["name" as NSObject] as! String
    let orderCount = orderJsonDict["order_count" as NSObject] as! Int
    let fullTitle = orderJsonDict["full_title" as NSObject] as! String
    
    return OrderDto(name: name, orderCount: orderCount, fullTitle: fullTitle)
}

func mapJsonToDeviceInfo(_ deviceInfoJsonDict: Dictionary<NSObject, AnyObject>) -> DeviceDto {
    let id = deviceInfoJsonDict["id" as NSObject] as! String
    let number = deviceInfoJsonDict["number" as NSObject] as! Int
    let vendorId = deviceInfoJsonDict["vendor_id" as NSObject] as! String
    
    return DeviceDto(id: id, number: number, vendorId: vendorId)
}

func receiptToParameters(_ receipt: Receipt, deviceId: String) -> [String: AnyObject] {
    let orders = receipt.orders
    let ordersParameters = orders.map { order in orderToParameters(order) }
    let receiptParameters: [String: AnyObject] = [
        "orders": ordersParameters as AnyObject,
        "device_id": deviceId as AnyObject
    ]
    
    return receiptParameters
}

func vendorIdParameters(_ vendorId: String) -> [String: AnyObject] {
    let vendorParam: [String: AnyObject] = [
        "vendor_id": vendorId as AnyObject
    ]
    
    return vendorParam
}

func orderToParameters(_ order: Order) -> [String: AnyObject] {
    var orderParameters: [String: AnyObject] = Dictionary()
    
    orderParameters["name"] = order.orderName as AnyObject?
    orderParameters["full_title"] = order.orderTitle as AnyObject?
    orderParameters["order_count"] = order.orderCount as AnyObject?
    
    return orderParameters
}

//example of data: "2016-01-20 12:04:27.396000", substring to dot
private func parseRemoteDateFromString(_ remoteDateStr: String) -> Date? {
    let dateFormatter = DateFormatter()
    let dateFormat = "yyyy-MM-dd HH:mm:ss"
    let dateWithoutMs = remoteDateStr.substring(to: remoteDateStr.characters.index(of: Character("."))!)
    dateFormatter.dateFormat = dateFormat
    
    let date = dateFormatter.date(from: dateWithoutMs)
    
    return date
}
