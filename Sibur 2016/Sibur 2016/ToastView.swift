//
//  ToastView.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/7/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.
//

import Foundation
import UIKit

class ShadowsConfig {
    let color: UIColor
    let opacity: Float
    let radius: Float
    let offset: CGSize
    
    init(color: UIColor, opacity: Float, radius: Float, offset: CGSize) {
        self.color = color
        self.opacity = opacity
        self.radius = radius
        self.offset = offset
    }
}

private let defaultShadowsConfig =
    ShadowsConfig(color: UIColor.black, opacity: 0.8, radius: 3.0, offset: CGSize(width: 2.0, height: 2.0))

func showToast(_ text: String, delay: Float) {
    let toastView = createToastView(text)
    
    setupToastViewStyle(toastView)
    showToastWithDelay(toastView, delay: delay)
}

private func createToastView(_ text: String) -> UIView {
    let toastLabel = createToastLabel(text)
    
    let toastViewWidth = CGFloat(toastLabel.frame.width + 20.0)
    let toastViewHeight = CGFloat(40)
    
    //TODO: change magic constants to some adequate relative value
    let toastView = UIView(frame: CGRect(x: (1024-toastViewWidth)/2, y: 700, width: toastViewWidth, height: toastViewHeight))
    toastView.backgroundColor = volumeTextColor
    
    toastLabel.frame.origin = calculateCenterPosition(toastLabel.frame.size, whereSizeCentered: toastView.frame.size)
    
    toastView.addSubview(toastLabel)
    
    return toastView
}

private func createToastLabel(_ text: String) -> UILabel {
    let label = UILabel()
    label.text = text
    label.textColor = UIColor.white
    label.backgroundColor = UIColor.clear
    label.sizeToFit()
    
    return label
}

private func setupToastViewStyle(_ toastView: UIView) {
    addCornersToView(toastView, cornerRadius: CGFloat(5))
    addShadowToView(toastView, shadowsConfig: defaultShadowsConfig)
}

private func showToastWithDelay(_ toast: UIView, delay: Float) {
    let realDelay = Int64(delay*Float(NSEC_PER_SEC))
    let dispatchTime = DispatchTime.now() + Double(realDelay) / Double(NSEC_PER_SEC)
    
    fadeViewOut(toast, fadeTime: 0, completion: nil)
    
    addToastToMainWindow(toast)
    
    fadeViewIn(toast, fadeTime: 0.5) { b in
        
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
            fadeViewOut(toast, fadeTime: 0.5) {
                b in toast.removeFromSuperview()
            }
        })
        
    }
}

private func fadeViewIn(_ view: UIView, fadeTime: Float, completion: ((Bool) -> ())?) {
    UIView.animate(withDuration: TimeInterval(fadeTime), animations: { view.alpha = CGFloat(1) }, completion: {
        b in if(completion != nil) {
            completion!(b)
        }
    }) 
}

private func fadeViewOut(_ view: UIView, fadeTime: Float, completion: ((Bool)->())?) {
    UIView.animate(withDuration: TimeInterval(fadeTime), animations: { view.alpha = CGFloat(0) }, completion: {
        b in if(completion != nil) {
            completion!(b)
        }
    }) 
}



private func addToastToMainWindow(_ toastView: UIView) {
    if let window = UIApplication.shared.delegate?.window {
        window?.addSubview(toastView)
    }
}

private func calculateCenterPosition(_ sizeBeingCentered: CGSize, whereSizeCentered: CGSize) -> CGPoint {
    let xPos = (whereSizeCentered.width - sizeBeingCentered.width)/2
    let yPos = (whereSizeCentered.height - sizeBeingCentered.height)/2
    
    return CGPoint(x: xPos, y: yPos)
}







