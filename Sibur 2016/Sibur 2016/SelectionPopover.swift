//  SelectionBox.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/4/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.

import Foundation
import UIKit

open class SelectionPopover {
    fileprivate let selectionCellDefaultSize = 44
    
    fileprivate let selectionTableController = UITableViewController()
    
    fileprivate let selectionDelegate = SelectionTableDelegate()
    fileprivate let selectionDataSource: SelectionTableDataSource
    fileprivate let sourceView: UIView
    
    fileprivate var selectionItems: [SelectableItem]
    
    var selectionStyle : SelectionStyle?
    
    init(sourceView: UIView, selectionItems: [SelectableItem]) {
        self.sourceView = sourceView
        self.selectionDataSource = SelectionTableDataSource(data: selectionItems)
        self.selectionItems = selectionItems
    }
    
    open func present(_ presenter: UIViewController, onSelection: ((SelectableItem) -> ())?) {
        setupSelectionTable()
        selectionDelegate.onRowSelectAction = createOnSelectionFunc(onSelection)
        setupPopover()
        presenter.present(selectionTableController, animated: true, completion: nil)
    }
    
    fileprivate func createOnSelectionFunc(_ onSelection: ((SelectableItem) -> ())?) -> (Int) -> () {
        return { [unowned self] idx in
            let item = self.selectionItems[idx]
            
            if(onSelection != nil) {
                onSelection!(item)
            }
            
            self.dismissSelectionPopover()
        }
    }
    
    fileprivate func setupSelectionTable() {
        setupTableStyle()
        setupTableLogic()
    }
    
    fileprivate func setupTableStyle() {
        let contentHeight = CGFloat(selectionCellDefaultSize * selectionItems.count)
        let additionalWidth = CGFloat(selectionStyle?.additionalWidth != nil ? selectionStyle!.additionalWidth! : 0)
        
        selectionTableController.modalPresentationStyle = UIModalPresentationStyle.popover
        selectionTableController.preferredContentSize = CGSize(width: 200 + additionalWidth, height: contentHeight)
        selectionTableController.tableView.isScrollEnabled = false
        selectionTableController.tableView.separatorStyle = .none
    }
    
    fileprivate func setupTableLogic() {
        selectionTableController.tableView.register(UITableViewCell.self, forCellReuseIdentifier: SelectionTableDataSource.selectionCellId)
        selectionTableController.tableView.dataSource = selectionDataSource
        selectionTableController.tableView.delegate = selectionDelegate
        selectionTableController.tableView.reloadData()
    }
    
    fileprivate func setupPopover() {
        let popoverPresentationController = selectionTableController.popoverPresentationController

        popoverPresentationController?.sourceView = sourceView
        popoverPresentationController?.sourceRect = CGRect(x: 0, y: 0, width: sourceView.frame.size.width, height: sourceView.frame.size.height)
        
        if let sStyle = selectionStyle {
            popoverPresentationController?.backgroundColor = sStyle.selectionBoxBackgroundColor
            selectionDataSource.cellBackgroundColor = sStyle.selectionBoxBackgroundColor
            selectionDataSource.cellTextColor = sStyle.selectionItemColor
            selectionDataSource.cellTextFont = sStyle.selectionItemFont
        }
    }
    
    fileprivate func dismissSelectionPopover() {
        selectionTableController.dismiss(animated: true, completion: nil)
    }
}
