//
//  SelectionStyle.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/5/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.
//

import Foundation
import UIKit

open class SelectionStyle {
    var selectionItemFont: UIFont?
    var selectionItemColor: UIColor?
    var selectionBoxBackgroundColor: UIColor?
    var additionalWidth : Int?
    
    init(itemFont: UIFont?, itemColor: UIColor?, boxBackgroundColor: UIColor?, additionalWidth: Int?) {
        self.selectionItemFont = itemFont
        self.selectionItemColor = itemColor
        self.selectionBoxBackgroundColor = boxBackgroundColor
        self.additionalWidth = additionalWidth
    }
}
