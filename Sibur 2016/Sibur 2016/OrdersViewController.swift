//
//  OrdersViewController.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/7/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.
//

import UIKit

//NOTE: god object for orders and tracking(not very good)
class OrdersViewController: UIViewController {
    fileprivate let cornerRadius = CGFloat(8)
    fileprivate let contentViewShadowsConfig = ShadowsConfig(color: UIColor.black,
        opacity: 0.8,
        radius: 8,
        offset: CGSize(width: 0, height: 0))
    
    fileprivate let ordersShowTreshold = CGFloat(600)
    fileprivate let ordersHideTreshold = CGFloat(180)

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var binButton: UIButton!
    @IBOutlet weak var orderTitle: UILabel!
    @IBOutlet weak var orderButton: BorderedButton!
    @IBOutlet weak var ordersTable: UITableView!
    @IBOutlet weak var remoteOrdersCollection: UICollectionView!
    @IBOutlet weak var waiterIdButton: UIButton!
    
    fileprivate var ordersViewHidedPosition: CGPoint?
    fileprivate var ordersViewShownPosition: CGPoint?
    fileprivate var isOrdersShown: Bool = false
    
    fileprivate var ordersDataSource: OrderDataSource?
    fileprivate var ordersDelegate: OrdersDelegate?
    
    fileprivate var remoteReceiptsDataSource: OrderStatusDataSource?
    fileprivate var remoteReceiptsDelegate: OrderStatusDelegate?
    
    let overlayView = UIView(frame: UIScreen.main.bounds)
    
    init() {
        super.init(nibName: "OrdersViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupData()
        setupWaiterIdButton()
        setupRemoteReceiptsTracking()
        subscribeForReceiptChanges()
        
        orderButton.addTarget(self, action: #selector(OrdersViewController.orderAction(_:)), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    fileprivate func setupView() {
        applyStyles()
        setupPosition()
        setupAppearence()
        setupOverlayView()
    }
    
    fileprivate func setupData() {
        ordersDataSource = OrderDataSource(ordersData: ReceiptService.sharedInstance.ordersDescriptions)
        ordersDelegate = OrdersDelegate()
        ordersTable.dataSource = ordersDataSource
        ordersTable.delegate = ordersDelegate
        ordersTable.register(OrderCell.cellNib, forCellReuseIdentifier: OrderCell.cellId)
    }
    
    fileprivate func setupWaiterIdButton() {
//        let deviceNumber = SetupDataService.getDeviceRegistration()!.number
        let deviceNumber = 0
        waiterIdButton.setTitle("Официант №\(deviceNumber)", for: UIControlState())
        waiterIdButton.addTarget(self, action: #selector(OrdersViewController.waiterAction(_:)), for: .touchUpInside)
    }
    
    fileprivate func setupRemoteReceiptsTracking() {
        let receiptTracker = RemoteReceiptTracker.sharedInstance
        remoteReceiptsDataSource = OrderStatusDataSource()
        remoteReceiptsDelegate = OrderStatusDelegate()
        
        remoteOrdersCollection.register(RemoteOrderCell.cellNib, forCellWithReuseIdentifier: RemoteOrderCell.cellId)
        remoteOrdersCollection.dataSource = remoteReceiptsDataSource
        remoteOrdersCollection.delegate = remoteReceiptsDelegate
        
        remoteOrdersCollection.reloadData()
        
        receiptTracker.subscribeForRemoteReceiptUpdates(self, onReceiptsUpdate: {
            [weak self]
            remoteReceipts in
            self?.remoteReceiptsDataSource?.remoteReceiptsData = remoteReceipts
            self?.remoteOrdersCollection.reloadData()
        })
        
        receiptTracker.updateRemoteReceipts(true)
    }
    
    fileprivate func subscribeForReceiptChanges() {
        let receiptService = ReceiptService.sharedInstance
        receiptService.subscribeForReceiptUpdates(self) {
            [weak self]
            ordersDescriptions in
            print("orders event triggered")
            self?.ordersDataSource?.ordersData = ordersDescriptions
            self?.ordersTable.reloadData()
        }
    }
    
    fileprivate func unSubscribeFromReceiptChanges() {
        let receiptService = ReceiptService.sharedInstance
        receiptService.unSubscribeFromReceiptUpdates(self)
    }

    fileprivate func applyStyles() {
        addCornersToView(contentView, cornerRadius: cornerRadius)
        addShadowToView(contentView, shadowsConfig: contentViewShadowsConfig)
        orderTitle.font = orderTitleFont
        orderButton.titleLabel?.font = orderButtonFont
        ordersTable.separatorStyle = .none
    }
    
    fileprivate func setupPosition() {
        let screenBounds = UIScreen.main.bounds
        let distanceFromOrdersToTopOfBinSticker = CGFloat(60)
        let ordersViewXPos = (screenBounds.width - view.frame.width) / 2
        
        ordersViewHidedPosition = CGPoint(x:ordersViewXPos, y:screenBounds.height - distanceFromOrdersToTopOfBinSticker)
        ordersViewShownPosition = CGPoint(x:ordersViewXPos, y:(screenBounds.height-view.frame.height)/2)
        
        view.frame.origin = ordersViewHidedPosition!
    }
    
    fileprivate func setupAppearence() {
        binButton.addGestureRecognizer(createPanGestureRecognizer())
        binButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(OrdersViewController.binTapped(_:))))
    }
    
    fileprivate func setupOverlayView() {
        overlayView.backgroundColor = UIColor.clear
        overlayView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(OrdersViewController.overlayTapped(_:))))
    }
    
    fileprivate func animateOrdersViewAppeareance(_ isShowing: Bool) {
        let destinationPosition = isShowing ? ordersViewShownPosition : ordersViewHidedPosition
        
        UIView.animate(withDuration: TimeInterval(0.5), animations:
            { self.view.frame.origin = destinationPosition! }, completion: onAppearenceAnimationCompletition)
    }
    
    fileprivate func onAppearenceAnimationCompletition(_ b : Bool) {
        self.isOrdersShown = self.view.frame.origin == self.ordersViewShownPosition
        updateOverlayStatus()
    }
    
    fileprivate func createPanGestureRecognizer() -> UIGestureRecognizer {
        let panGestureRec = UIPanGestureRecognizer()
        panGestureRec.addTarget(self, action: #selector(OrdersViewController.binDragged(_:)))
        
        return panGestureRec
    }
    
    func binTapped(_ gestureRec: UIGestureRecognizer!) {
        animateOrdersViewAppeareance(!isOrdersShown)
    }
    
    func binDragged(_ gestureRec: UIGestureRecognizer!) {
        let panGestureRec = gestureRec as! UIPanGestureRecognizer
        
        if(gestureRec.state == .changed) {
            doOrdersTranslation(panGestureRec)
        }
        
        if(gestureRec.state == .ended) {
            finishOrdersTranslation()
        }
    }
    
    func overlayTapped(_ gestureRec: UIGestureRecognizer!) {
        animateOrdersViewAppeareance(false)
    }
    
    func orderAction(_ sender: UIButton!) {
        ReceiptService.sharedInstance.pushReceipt {
            receiptPushStatus in
            
            switch receiptPushStatus {
            case .success:
                showSimpleAlert("Заказ принят. Ожидайте готовности.")
                RemoteReceiptTracker.sharedInstance.updateRemoteReceipts(true)
                
            case let .error(message):
                if let m = message {
                    showSimpleAlert(m)
                }
                else {
                    showSimpleAlert("Что-то пошло не так при выполнении заказа. Обратитесь к администратору")
                }
            }
        }
    }
    
    func waiterAction(_ sender: UIButton!) {
        let setupVc = SetupViewController()
        self.present(setupVc, animated: true, completion: nil)
    }
    
    fileprivate func doOrdersTranslation(_ panGestureRecognizer: UIPanGestureRecognizer) {
        if(!ordersViewOutOfBounds()) {
            let ordersViewOrigin = view.frame.origin
            let translationY = (panGestureRecognizer.translation(in: view).y)
            
            view.frame.origin = (CGPoint(x: (ordersViewOrigin.x), y:(ordersViewOrigin.y) + translationY))
        }
        
        panGestureRecognizer.setTranslation(CGPoint.zero, in: view)
    }
    
    fileprivate func finishOrdersTranslation() {
        let ordersOriginY = view.frame.origin.y
        animateOrdersViewAppeareance(isOrdersShown ? !(ordersOriginY > ordersHideTreshold) : ordersOriginY <= ordersShowTreshold)
    }
    
    fileprivate func ordersViewOutOfBounds() -> Bool {
        let origin = view.frame.origin
        
        return origin.y < ordersViewShownPosition!.y || origin.y > ordersViewHidedPosition!.y
    }
    
    fileprivate func updateOverlayStatus() {
        if(self.isOrdersShown) {
            self.addOverlayToDisableInput()
        } else {
            self.removeOverlayToEnableInput()
        }
    }
    
    fileprivate func addOverlayToDisableInput() {
        self.view.superview?.addSubview(overlayView)
        self.view.superview?.bringSubview(toFront: view)
    }
    
    fileprivate func removeOverlayToEnableInput() {
        overlayView.removeFromSuperview()
    }
}
