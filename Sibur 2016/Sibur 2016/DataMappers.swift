//
//  DataMappers.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/6/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.
//

import Foundation
import UIKit

let cocktailNames = ["happiness", "interest", "joy", "pleasure"]
let beerName = ["serenity_white", "serenity_black"]

let cocktailsInfoText =
            [
                "1. Композиция для O2\n2. Вишневый сок",
                "1. Водка\n2. Блю курасао ликер\n3. Гранатовый сироп\n4. Вода без газа\n5. Лактат кальция\n6. Альгинат натрия",
                "1. Манговый смузи",
                "1. Клубничное мороженое\n2. Клубника\n3. Лопающиеся шарики манго"
]

func mapCocktailToInfoText(_ cocktailName: String) -> String {
    if let idx = cocktailNames.index(of: cocktailName) {
        return cocktailsInfoText[Int(idx)]
    }
    
    return ""
}
