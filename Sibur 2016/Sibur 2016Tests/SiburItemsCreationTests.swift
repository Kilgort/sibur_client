//
//  SiburItems.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 12/16/15.
//  Copyright © 2015 Oleg Tyshchenko. All rights reserved.
//

import XCTest
@testable import Sibur_2016

func isAll<T>(_ collection: [T], predicate: (T) -> Bool) -> Bool {
    if(collection.count == 0) { return true }
    if(!predicate(collection.first!)) { return false }
    
    let tail = Array(collection.dropFirst())
    
    return isAll(tail, predicate: predicate)
}

class SiburItemsCreationTests: XCTestCase {
    func testCocktailsShouldHaveCorrectTitle() {
        let happinessCocktail = createCocktail(CocktailNames.happiness)
        let interestCocktail = createCocktail(CocktailNames.interest)
        let joyCocktail = createCocktail(CocktailNames.joy)
        let pleasureCocktail = createCocktail(CocktailNames.pleasure)
        
        XCTAssertEqual(happinessCocktail?.fullTitle, "Счастье", "wrong full title - happiness cocktail")
        XCTAssertEqual(interestCocktail?.fullTitle, "Интерес", "wrong full title - interest cocktail")
        XCTAssertEqual(joyCocktail?.fullTitle, "Радость", "wrong full title - joy cocktail")
        XCTAssertEqual(pleasureCocktail?.fullTitle, "Удовольствие", "wrong full title - pleasure cocktail")
    }
    
    func testBeerShouldHaveCorrectTitle() {
        let lightBeer = createBeer(BeerNames.lightBeer)
        let blackBeer = createBeer(BeerNames.darkBeer)
        
        XCTAssertEqual(lightBeer?.fullTitle, "Безмятежность светлое", "wrong full title - joy cocktail")
        XCTAssertEqual(blackBeer?.fullTitle, "Безмятежность темное", "wrong full title - joy cocktail")
    }
    
    func testCoffeePlainHaveCorrectTitle() {
        let plainLatte = createCoffee("latte", withSugar: false, withMilk: false, syrup: .None)
        let plainCappucino = createCoffee("cappucino", withSugar: false, withMilk: false, syrup: .None)
        let plainEspresso = createCoffee("espresso", withSugar: false, withMilk: false, syrup: .None)
        let plainAmericano = createCoffee("americano", withSugar: false, withMilk: false, syrup: .None)
        let plainRaf = createCoffee("raf", withSugar: false, withMilk: false, syrup: .None)

        XCTAssertEqual(plainLatte?.fullTitle, "Латтэ", "wrong full title - latte")
        XCTAssertEqual(plainCappucino?.fullTitle, "Капучино", "wrong full title - cappucino")
        XCTAssertEqual(plainEspresso?.fullTitle, "Эспрессо", "wrong full title - espresso")
        XCTAssertEqual(plainAmericano?.fullTitle, "Американо", "wrong full title - americano")
        XCTAssertEqual(plainRaf?.fullTitle, "Раф", "wrong full title - raf")
    }
    
    func testCoffeeWithSyrupHaveCorrectTitle() {
        let syrupCaramelLatte = createCoffee("latte", withSugar: false, withMilk: false, syrup: .Caramel)
        let syrupNoneCappucino = createCoffee("cappucino", withSugar: false, withMilk: false, syrup: .None)
        let syrupVanilEspresso = createCoffee("espresso", withSugar: false, withMilk: false, syrup: .Vanil)
        let syrupChocolateAmericano = createCoffee("americano", withSugar: false, withMilk: false, syrup: .Chocolate)
        let syrupWalnutRaf = createCoffee("raf", withSugar: false, withMilk: false, syrup: .Walnut)
        
        XCTAssertEqual(syrupCaramelLatte?.fullTitle, "Латтэ с карамельным сиропом", "wrong full title - latte with caramel")
        XCTAssertEqual(syrupNoneCappucino?.fullTitle, "Капучино", "wrong full title - cappucino")
        XCTAssertEqual(syrupVanilEspresso?.fullTitle, "Эспрессо с ванильным сиропом", "wrong full title - espresso with vanil")
        XCTAssertEqual(syrupChocolateAmericano?.fullTitle, "Американо с шоколадным сиропом", "wrong full title - americano with chocolate")
        XCTAssertEqual(syrupWalnutRaf?.fullTitle, "Раф с ореховым сиропом", "wrong full title - americano with chocolate")
    }
    
    func testCoffeeWithMilkHaveCorrectTitle() {
        let milkLatte = createCoffee("latte", withSugar: false, withMilk: true, syrup: .None)
        let milkCappucino = createCoffee("cappucino", withSugar: false, withMilk: true, syrup: .None)
        let milkEspresso = createCoffee("espresso", withSugar: false, withMilk: true, syrup: .None)
        let milkAmericano = createCoffee("americano", withSugar: false, withMilk: true, syrup: .None)
        let milkRaf = createCoffee("raf", withSugar: false, withMilk: true, syrup: .None)
        
        XCTAssertEqual(milkLatte?.fullTitle, "Латтэ с молоком", "wrong full title - latte with milk")
        XCTAssertEqual(milkCappucino?.fullTitle, "Капучино с молоком", "wrong full title - cappucino with milk")
        XCTAssertEqual(milkEspresso?.fullTitle, "Эспрессо с молоком", "wrong full title - espresso with milk")
        XCTAssertEqual(milkAmericano?.fullTitle, "Американо с молоком", "wrong full title - americano with milk")
        XCTAssertEqual(milkRaf?.fullTitle, "Раф с молоком", "wrong full title - raf with milk")
    }
    
    func testCoffeeWithSugarHaveCorrectTitle() {
        let sugarLatte = createCoffee("latte", withSugar: true, withMilk: false, syrup: .None)
        let sugarCappucino = createCoffee("cappucino", withSugar: true, withMilk: false, syrup: .None)
        let sugarEspresso = createCoffee("espresso", withSugar: true, withMilk: false, syrup: .None)
        let sugarAmericano = createCoffee("americano", withSugar: true, withMilk: false, syrup: .None)
        let sugarRaf = createCoffee("raf", withSugar: true, withMilk: false, syrup: .None)
        
        XCTAssertEqual(sugarLatte?.fullTitle, "Латтэ с сахаром", "wrong full title - latte with sugar")
        XCTAssertEqual(sugarCappucino?.fullTitle, "Капучино с сахаром", "wrong full title - cappucino with sugar")
        XCTAssertEqual(sugarEspresso?.fullTitle, "Эспрессо с сахаром", "wrong full title - espresso with sugar")
        XCTAssertEqual(sugarAmericano?.fullTitle, "Американо с сахаром", "wrong full title - americano with sugar")
        XCTAssertEqual(sugarRaf?.fullTitle, "Раф с сахаром", "wrong full title - raf with sugar")
    }
    
    func testCoffeeWithMilkAndSugarHaveCorrectTitle() {
        let milkSugarLatte = createCoffee("latte", withSugar: true, withMilk: true, syrup: .None)
        let milkSugarCappucino = createCoffee("cappucino", withSugar: true, withMilk: true, syrup: .None)
        let milkSugarEspresso = createCoffee("espresso", withSugar: true, withMilk: true, syrup: .None)
        let milkSugarAmericano = createCoffee("americano", withSugar: true, withMilk: true, syrup: .None)
        let milkSugarRaf = createCoffee("raf", withSugar: true, withMilk: true, syrup: .None)
        
        XCTAssertEqual(milkSugarLatte?.fullTitle, "Латтэ с молоком и сахаром", "wrong full title - latte with milk and sugar")
        XCTAssertEqual(milkSugarCappucino?.fullTitle, "Капучино с молоком и сахаром", "wrong full title - cappucino with milk and sugar")
        XCTAssertEqual(milkSugarEspresso?.fullTitle, "Эспрессо с молоком и сахаром", "wrong full title - espresso with milk and sugar")
        XCTAssertEqual(milkSugarAmericano?.fullTitle, "Американо с молоком и сахаром", "wrong full title - americano with milk and sugar")
        XCTAssertEqual(milkSugarRaf?.fullTitle, "Раф с молоком и сахаром", "wrong full title - raf with milk and sugar")
    }
    
    func testCoffeeWithMilkSugarAndSyrupHaveCorrectTitle() {
        let milkSugarSyrupLatte = createCoffee("latte", withSugar: true, withMilk: true, syrup: .Caramel)
        let milkSugarSyrupCappucino = createCoffee("cappucino", withSugar: true, withMilk: true, syrup: .Vanil)
        let milkSugarSyrupEspresso = createCoffee("espresso", withSugar: true, withMilk: true, syrup: .Chocolate)
        let milkSugarSyrupAmericano = createCoffee("americano", withSugar: true, withMilk: true, syrup: .Walnut)
        let milkSugarSyrupRaf = createCoffee("raf", withSugar: true, withMilk: true, syrup: .Caramel)
        
        XCTAssertEqual(milkSugarSyrupLatte?.fullTitle,
            "Латтэ с карамельным сиропом, молоком и сахаром", "wrong full title - latte with milk and sugar, syrup")
        
        XCTAssertEqual(milkSugarSyrupCappucino?.fullTitle,
            "Капучино с ванильным сиропом, молоком и сахаром", "wrong full title - cappucino with milk and sugar, syrup")
        
        XCTAssertEqual(milkSugarSyrupEspresso?.fullTitle,
            "Эспрессо с шоколадным сиропом, молоком и сахаром", "wrong full title - espresso with milk and sugar, syrup")
        
        XCTAssertEqual(milkSugarSyrupAmericano?.fullTitle,
            "Американо с ореховым сиропом, молоком и сахаром", "wrong full title - americano with milk and sugar, syrup")
        
        XCTAssertEqual(milkSugarSyrupRaf?.fullTitle,
            "Раф с карамельным сиропом, молоком и сахаром", "wrong full title - raf with milk and sugar, syrup")
    }
    
    func testCoffeeWithMilkAndSyrupHaveCorrectTitle() {
        let milkSyrupLatte = createCoffee("latte", withSugar: false, withMilk: true, syrup: .Caramel)
        let milkSyrupCappucino = createCoffee("cappucino", withSugar: false, withMilk: true, syrup: .Vanil)
        let milkSyrupEspresso = createCoffee("espresso", withSugar: false, withMilk: true, syrup: .Chocolate)
        let milkSyrupAmericano = createCoffee("americano", withSugar: false, withMilk: true, syrup: .Walnut)
        let milkSyrupRaf = createCoffee("raf", withSugar: false, withMilk: true, syrup: .Caramel)
        
        XCTAssertEqual(milkSyrupLatte?.fullTitle, "Латтэ с карамельным сиропом и молоком", "wrong full title - latte with milk and syrup")
        XCTAssertEqual(milkSyrupCappucino?.fullTitle, "Капучино с ванильным сиропом и молоком", "wrong full title - cappucino with milk and syrup")
        XCTAssertEqual(milkSyrupEspresso?.fullTitle, "Эспрессо с шоколадным сиропом и молоком", "wrong full title - espresso with milk and syrup")
        XCTAssertEqual(milkSyrupAmericano?.fullTitle, "Американо с ореховым сиропом и молоком", "wrong full title - americano with milk and syrup")
        XCTAssertEqual(milkSyrupRaf?.fullTitle, "Раф с карамельным сиропом и молоком", "wrong full title - raf with milk and syrup")
    }
    
    func testCoffeeWithSugarAndSyrupHaveCorrectTitle() {
        let sugarSyrupLatte = createCoffee("latte", withSugar: true, withMilk: false, syrup: .Caramel)
        let sugarSyrupCappucino = createCoffee("cappucino", withSugar: true, withMilk: false, syrup: .Vanil)
        let sugarSyrupEspresso = createCoffee("espresso", withSugar: true, withMilk: false, syrup: .Chocolate)
        let sugarSyrupAmericano = createCoffee("americano", withSugar: true, withMilk: false, syrup: .Walnut)
        let sugarSyrupRaf = createCoffee("raf", withSugar: true, withMilk: false, syrup: .Caramel)
        
        XCTAssertEqual(sugarSyrupLatte?.fullTitle, "Латтэ с карамельным сиропом и сахаром", "wrong full title - latte with sugar and syrup")
        XCTAssertEqual(sugarSyrupCappucino?.fullTitle, "Капучино с ванильным сиропом и сахаром", "wrong full title - cappucino with sugar and syrup")
        XCTAssertEqual(sugarSyrupEspresso?.fullTitle, "Эспрессо с шоколадным сиропом и сахаром", "wrong full title - espresso with sugar and syrup")
        XCTAssertEqual(sugarSyrupAmericano?.fullTitle, "Американо с ореховым сиропом и сахаром", "wrong full title - americano with sugar and syrup")
        XCTAssertEqual(sugarSyrupRaf?.fullTitle, "Раф с карамельным сиропом и сахаром", "wrong full title - raf with sugar and syrup")
    }
    
    func testTeaPlainHaveCorrectTitle() {
        let plainBlackTea = createTea("black_classic", withSugar: false)
        let plainBlackChebTea = createTea("black_cheb", withSugar: false)
        let plainGreenTea = createTea("green_classic", withSugar: false)
        let plainGreenRomTea = createTea("green_rom", withSugar: false)

        
        XCTAssertEqual(plainBlackTea?.fullTitle, "Чай черный классический", "wrong full title - black tea")
        XCTAssertEqual(plainBlackChebTea?.fullTitle, "Чай черный с чабрецом", "wrong full title - black cheb tea")
        XCTAssertEqual(plainGreenTea?.fullTitle, "Чай зеленый классический", "wrong full title - black cheb tea")
        XCTAssertEqual(plainGreenRomTea?.fullTitle, "Чай зеленый с ромашкой", "wrong full title - green tea")
    }
    
    func testTeaSugarHaveCorrectTitle() {
        let sugarBlackTea = createTea("black_classic", withSugar: true)
        let sugarBlackChebTea = createTea("black_cheb", withSugar: true)
        let sugarGreenTea = createTea("green_classic", withSugar: true)
        let sugarGreenRomTea = createTea("green_rom", withSugar: true)
        
        
        XCTAssertEqual(sugarBlackTea?.fullTitle, "Чай черный классический с сахаром", "wrong full title - black tea")
        XCTAssertEqual(sugarBlackChebTea?.fullTitle, "Чай черный с чабрецом и сахаром", "wrong full title - black cheb tea")
        XCTAssertEqual(sugarGreenTea?.fullTitle, "Чай зеленый классический с сахаром", "wrong full title - black cheb tea")
        XCTAssertEqual(sugarGreenRomTea?.fullTitle, "Чай зеленый с ромашкой и сахаром", "wrong full title - green tea")
    }
    
    func testChocolateHaveCorrectTitle() {
        let chocoMilk = createChocolate("choco_milk")
        let chocoLemon = createChocolate("choco_lemon")
        let chocoBlack = createChocolate("choco_black")
        let chocoOrange = createChocolate("choco_orange")
        let chocoHazelnut = createChocolate("choco_hazelnut")
        
        XCTAssertEqual(chocoMilk?.fullTitle, "Шоколад молочный", "wrong full title - milk choco")
        XCTAssertEqual(chocoLemon?.fullTitle, "Шоколад горький с цукатами лимона и имбиря", "wrong full title - lemon choco")
        XCTAssertEqual(chocoBlack?.fullTitle, "Шоколад горький", "wrong full title - black choco")
        XCTAssertEqual(chocoOrange?.fullTitle, "Шоколад горький с цукатами апельсинами и лепестками миндаля", "wrong full title - orange choco")
        XCTAssertEqual(chocoHazelnut?.fullTitle, "Шоколад молочный с дробленым фундуком", "wrong full title - hazelnut choco")
    }
}
















