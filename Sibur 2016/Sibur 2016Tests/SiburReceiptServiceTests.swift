//
//  SiburReceiptServiceTests.swift
//  Sibur 2016
//
//  Created by Oleg Tyshchenko on 1/16/16.
//  Copyright © 2016 Oleg Tyshchenko. All rights reserved.
//

import XCTest

@testable import Sibur_2016

class SiburReceiptServiceTests: XCTestCase {
    fileprivate var receiptService: ReceiptService = ReceiptService.sharedInstance
    fileprivate var cocktailMockItem = createCocktail(CocktailNames.joy)!
    fileprivate var teaMockItem = createTea(TeaNames.blackTea, withSugar: false)!
    
    fileprivate var orderDescriptionLastState: [OrderDescription]?

    override func setUp() {
        receiptService.subscribeForReceiptUpdates(self, onReceiptUpdate: {
            [weak self]
            od in self?.orderDescriptionLastState = od
        })
    }
    
    override func tearDown() {
        receiptService.clearOrders()
        receiptService.unSubscribeFromReceiptUpdates(self)
    }
    
    func testOrderIsAddedToReceipt() {
        receiptService.addItemToReceipt(cocktailMockItem)
        receiptService.addItemToReceipt(teaMockItem)
        XCTAssertEqual(orderDescriptionLastState?.count, 2)

        let firstDescription = orderDescriptionLastState![0]
        let secondDescription = orderDescriptionLastState![1]
        XCTAssertEqual(firstDescription.orderCount, 1)
        XCTAssertEqual(secondDescription.orderCount, 1)

        XCTAssertEqual(firstDescription.orderTitle, "Радость")
        XCTAssertEqual(secondDescription.orderTitle, "Чай черный классический")
    }
    
    func testOrderIfDuplicateIsAddedIncrementOrderCount() {
        receiptService.addItemToReceipt(cocktailMockItem)
        receiptService.addItemToReceipt(cocktailMockItem)
        
        let firstDescription = orderDescriptionLastState![0]
        
        XCTAssertEqual(orderDescriptionLastState?.count, 1)
        XCTAssertEqual(firstDescription.orderCount, 2)
    }

    func testOrderIsDeletedFromReceipt() {
        receiptService.addItemToReceipt(teaMockItem)
        
        XCTAssertEqual(orderDescriptionLastState?.count, 1)
        receiptService.decrementItemAtIndex(0)
        XCTAssertEqual(orderDescriptionLastState?.count, 0)
    }

    func testOrderIsIncrementedInReceipt() {
        receiptService.addItemToReceipt(teaMockItem)
        
        XCTAssertEqual(orderDescriptionLastState?[0].orderCount, 1)
        receiptService.incrementItemAtIndex(0)
        XCTAssertEqual(orderDescriptionLastState?[0].orderCount, 2)
    }

    func testOrderIsDecrementedInReceipt() {
        receiptService.addItemToReceipt(teaMockItem)
        
        XCTAssertEqual(orderDescriptionLastState?[0].orderCount, 1)
        receiptService.incrementItemAtIndex(0)
        XCTAssertEqual(orderDescriptionLastState?[0].orderCount, 2)
        receiptService.decrementItemAtIndex(0)
        XCTAssertEqual(orderDescriptionLastState?[0].orderCount, 1)
    }
}
